# <odp-event-registration-form>

> A component to register for events on ODP.

`<odp-event-registration-form>` is a complex component that can be configured to accept event data from ODP and provide a registration form. It generates and object that can be sent back to ODP to register the user.

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/odp-event-registration-form
```
## Usage
```
import '@orcden/odp-event-registration-form';
```
```
<script>
    function httpGet( theUrl ) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
        xmlHttp.send( null );
        return JSON.parse( xmlHttp.responseText );
    }

    document.querySelector( '#form' ).eventData = httpGet( 'YOUR ODP EVENT ENDPOINT HERE' );
</script>

<odp-event-registration-form id='form' inline is-member></odp-event-registration-form>
```
    
## Attributes
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `inline`  | Boolean | true   | Useful for mobile, set to false to decrease width. |
| `is-member`  | Boolean | false   | Used to set member prices display and calculation |
| `closed`  | Boolean | false   | Can be used to manually display the registration closed message. |

## Properties
| Attribute | Type | Default | Description                                                                             |
|-----------|---------|---------|-----------------------------------------------------------------------------------------|
| `eventData`  | Object  | {}      | Get. Set. The form requires this to build itself.  |
| `inline`  | Boolean | true   | Useful for mobile, set to false to decrease width. |
| `isMember`  | Boolean | false   | Used to set member prices display and calculation |
| `invalidMessage`  | String | ""   | Get Only, used with validation; if the form fails display this. |
| `closed`  | Boolean | false   | Can be used to manually display the registration closed message. |

## Functions
| Name | Parameters | Description                                  |
|-----------|------|-----------------------------------------------|
| `getData`   | None | Get the event registration data |
| `validate`   | None | Validates if the required info has been filled out |

## Styling
| Shadow Parts     | Description           |
|------------------|-----------------------|
| ex. mode-item-title           | The div used to house the title slot |
| ...          | Find the associated part in the DOM for now; docs to be updated |

## Development
### Run development server and show demo

```
npm run demo
```

### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Run tests

```
npm run test
```

### Build for production

```
npm run build
```
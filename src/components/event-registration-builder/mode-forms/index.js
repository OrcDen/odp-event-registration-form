import { OdpEventParentItemGroupRegistration } from "./ParentItemGroupRegistration.js";
import { OdpEventParentSingleRegistration } from "./ParentSingleRegistration.js";
import { OdpEventParentMultiRegistration } from "./ParentMultiRegistration.js";
import { OdpEventItemRegistration } from "./ItemRegistration.js";
import { OdpEventVarliableRegistration } from "./VariableRegistration.js";
import { OdpEventAttendeeGroupRegistration } from "./AttendeeGroupRegistration.js";
import { OdpEventAttendeeRegistration } from "./AttendeeRegistration.js";

if ( !customElements.get( "odp-event-parent-item-group-registration" ) ) {
    window.customElements.define( "odp-event-parent-item-group-registration", OdpEventParentItemGroupRegistration );
}

if ( !customElements.get( "odp-event-parent-single-registration" ) ) {
    window.customElements.define( "odp-event-parent-single-registration", OdpEventParentSingleRegistration );
}

if ( !customElements.get( "odp-event-parent-multi-registration" ) ) {
    window.customElements.define( "odp-event-parent-multi-registration", OdpEventParentMultiRegistration );
}

if ( !customElements.get( "odp-event-item-registration" ) ) {
    window.customElements.define( "odp-event-item-registration", OdpEventItemRegistration );
}

if ( !customElements.get( "odp-event-variable-registration" ) ) {
    window.customElements.define( "odp-event-variable-registration", OdpEventVarliableRegistration );
}

if ( !customElements.get( "odp-event-attendee-group-registration" ) ) {
    window.customElements.define( "odp-event-attendee-group-registration", OdpEventAttendeeGroupRegistration );
}

if ( !customElements.get( "odp-event-attendee-registration" ) ) {
    window.customElements.define( "odp-event-attendee-registration", OdpEventAttendeeRegistration );
}
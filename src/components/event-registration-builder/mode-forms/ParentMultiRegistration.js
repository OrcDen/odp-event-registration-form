import ModeBuilder from './lib/ModeBuilder.js';
import ODEventParentRegistrationMode from './lib/eventParentRegistrationMode.js';
import '@orcden/od-forms';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        :host {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        #parent-multi-registration {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        :host( :not( [inline] ) ) .indent {
            padding-left: 0;
        }

        :host( :not( [inline] ) ) .item {
            padding-left: 0;
        }

        :host( :not( [inline] ) ) #items-container {
            padding-left: 0;
        }

        :host( :not( [show-checkbox] ) ) #title-checkbox {
            display: none;
        }

        .container {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 1rem 1rem;
        }

        .indent {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding-left: 2rem;
            margin-bottom: 1rem;
            border-left: 1px solid darkgrey;
        }

        #title-container {
            display: flex;
            align-items: center;
        }
    
        #title {
            font-weight: 600;
            font-size: 1.25rem;
            margin: 0 1rem;
        }

        #items-container {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding-left: 2rem;
        }

        #options-container {
            display: none;
        }

    </style>
    
    <div id="parent-multi-registration">
        <div id='title-container'>
            <input id='title-checkbox' type='checkbox' part='mode-checkbox'>
            <label id='title'  part='mode-item-group-title'></label>
        </div>
        <div class='indent'>
            <div id='items-container' class='container'>
                <slot name='items' id='items'></slot>
            </div>    
            <button id='add-button' part='mode-add-button'></button>
        </div>
        <div id='options-container'>
            <slot id='options'></slot>
        </div>
    </div>         
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "odp-event-parent-multi-registration" );

export class OdpEventParentMultiRegistration extends ODEventParentRegistrationMode {
    constructor() {
        super( template );
    }

    _init() {
        this._modeBuilder = new ModeBuilder( this );

        this._children = [];
        this._internalCount = 0;

        this._options = [];

        this._titleLabel = this.shadowRoot.querySelector( '#title' );
        this._titleCheckbox = this.shadowRoot.querySelector( '#title-checkbox' );

        this._itemsSlot = this.shadowRoot.querySelector( '#items' );
        this._optionsSlot = this.shadowRoot.querySelector( '#items' );
        this._addButton = this.shadowRoot.querySelector( '#add-button' );

        this._invalidMessage = '';
    }

    connectedCallback() {
        super.connectedCallback();        

        //set attribute default values first and pass to element

        //all properties should be upgraded to allow lazy functionality

        //listeners and others etc.

        let config = { attributes: false, childList: true, subtree: true };
        this._observer = new MutationObserver( ( mutationsList, observer ) => {        
            for( let i = 0; i < mutationsList.length; i++ ) {
                let record = mutationsList[i];
                if( record.addedNodes && record.addedNodes.length ) {
                    for( let j = 0; j < record.addedNodes.length; j++ ) {
                        let node = record.addedNodes[j];
                        if( node.parentElement !== this ) {
                            continue;
                        }
                        if( node.tagName.includes( 'ODP-EVENT') ) {
                            this._options.push( node );
                            this._updateAllOptions( node );
                        }
                        if( node.hasAttribute( 'slot' ) && node.classList.contains( 'item-container' ) ) {
                            this._children.push( node );
                            this._buildChildOptions( node );
                            this._internalCount += 1;
                        }
                    
                    }
                }
                if( record.removedNodes && record.removedNodes.length ) {
                    for( let j = 0; j < record.removedNodes.length; j++ ) {
                        let node = record.removedNodes[j];
                        if( node.tagName.includes( 'ODP-EVENT') ) {                       
                            for( let k = 0; k < this._children.length; k++ ) {
                                if( node === this._children[k] ) {
                                    found = true;
                                    this._children.splice( k, 1 );
                                    break;
                                }
                            }
                        }
                    }
                }
            }            
            this._reIndexItems();
            this._mutationObserverCallback( mutationsList, observer );  
            this._dispatchChange();  
        } );
        this._observer.observe( this, config );

        this._requestInlineHook();

        this.setAttribute( 'exportparts', 'mode-radio, mode-item-group-title, mode-add-button, mode-qty-input, mode-qty-title, mode-item-title, mode-price, mode-attendee-group-title, mode-attendee-title, divider, mode-checkbox, mode-delete-button, mode-attendee-form, mode-attendee-table, mode-attendee-table-row, mode-attendee-table-cell, mode-attendee-label-cell, mode-attendee-input-cell, mode-attendee-field-label, mode-attendee-field-label-row, mode-attendee-value-label, mode-attendee-form-input, mode-attendee-form-select, mode-attendee-form-textarea, mode-attendee-form-richtext, mode-attendee-richtext-editor, mode-multi-form, mode-multi-table, mode-multi-table-row, mode-multi-table-cell, mode-multi-label-cell, mode-multi-input-cell, mode-multi-field-label, mode-multi-field-label-row, mode-multi-value-label, mode-multi-form-input, mode-multi-form-select, mode-multi-form-textarea, mode-multi-form-richtext, mode-multi-richtext-editor' );

        this._dispatchChange();
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return [
            'event-id',
            'registration-id',
            'is-member',
            'show-checkbox',
            'checked',
            'title',
            'inline'
        ];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        super.attributeChangedCallback( attrName, oldValue, newValue );
        this._dispatchChange();
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////
    
    get invalidMessage() {
        return this._invalidMessage;
    }

    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    
    _mutationObserverCallback( mutationsList, observer ) {
        this.dispatchEvent(
            new CustomEvent( 'odp-event-multi-mutation', { 
                bubbles: true, 
                detail: { 
                    'mutationsList': mutationsList,
                    'observer': observer
                }
            } )
        );
    }

    mutationPromiseCallback( e, resolve ) {
        e.stopPropagation();
        this.removeEventListener( 'odp-event-multi-mutation', ( e ) => { this.mutationPromiseCallback( e, resolve ); } );
        resolve( e.detail ); 
    }    

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////

    _buildNewItem( itemTitle ) {
        let newItem = this._buildItemContainer( itemTitle );
        let container = newItem.firstChild.nextSibling;
        container.insertBefore( this._buildFormSelect(), container.firstChild );
        return newItem;
    }

    _buildItemContainer( title ) {
        let container = document.createElement( 'div' );
        container.id = title;
        container.classList.add( 'item-container' );
        container.style.width = '100%';
        container.style.paddingLeft = '1rem';
        container.setAttribute( 'slot', 'items' );

        let line = document.createElement( 'hr' );
        line.style.display = 'flex';
        line.style.width = '100%';
        line.setAttribute( 'part', 'divider' );
        container.appendChild( line );

        let inner = document.createElement( 'div' );
        inner.classList.add( 'form-container' );
        inner.style.display = 'flex';
        inner.style.alignItems = 'flex-start';

        container.appendChild( inner );
        inner.appendChild( this._buildDeleteButton( title ) );

        let item = document.createElement( 'div' );
        item.classList.add( 'item' );
        container.appendChild( item );

        return container;
    }

    _buildFormSelect() {
        let form = document.createElement( 'od-form' );
        form.style.width = '100%';
        form.setAttribute( 'exportparts', 'form : mode-multi-form, table : mode-multi-table, table-row : mode-multi-table-row, table-cell : mode-multi-table-cell, label-cell : mode-multi-label-cell, input-cell : mode-multi-input-cell, field-label : mode-multi-field-label, field-label-row : mode-multi-field-label-row, value-label : mode-multi-value-label, form-input : mode-multi-form-input, form-select : mode-multi-form-select, form-textarea : mode-multi-form-textarea, form-richtext : mode-multi-form-richtext, richtext-editor : mode-multi-richtext-editor' );
        
        let selectlist = document.createElement( 'od-form-selectlist' );
        
        let label = document.createElement( 'label' );
        label.setAttribute( 'slot', 'label' );
        label.innerHTML = 'Select Option:';

        selectlist.appendChild( this._buildOption( '', '-' ) );
        selectlist.appendChild( label );

        form.appendChild( selectlist );
        selectlist.setAttribute( 'name', 'optionType' );
        selectlist.setAttribute( 'required', true );

        this.dispatchEvent( new CustomEvent( 'add-inline-hook', { bubbles: true, composed: true, 'detail': selectlist } ) );

        form.addEventListener( 'od-form-change', ( e ) => this._optionChanged( e ) );

        return form;
    }

    _buildChildOptions( node ) {
        let selectlist = node.querySelector( 'od-form > od-form-selectlist' );
        for( let i = 0; i < this._options.length; i++ ) {
            let option = this._options[i];
            let title = option.title;
            selectlist.appendChild( this._buildOption( title, title ) );
        }
    }

    _buildDeleteButton( title ) {
        let button = document.createElement( 'button' );
        button.setAttribute( 'part', 'mode-delete-button' );
        button.innerHTML = 'X';
        button.id = title;
        button.style.margin = '.5rem 1rem';
        button.onclick = ( e ) => {
            let index = Number.parseInt( e.target.id )
            this._registrationInfo.childRegistrations.splice( index, 1 );
            this._children.splice( index, 1 );
            this._internalCount -= 1;
            button.parentElement.parentElement.remove();
        }
        return button;
    }

    _reIndexItems() {
        for( let i = 0; i < this._children.length; i++ ) {
            let child = this._children[i];
            child.id = i;
            child.querySelector( 'button' ).id = i;
        }
    }

    _updateAllOptions( node ) {
        let title = node.title;
        if( !title ) {
            return;
        }
        for( let i = 0; i < this._children.length; i++ ) {
            let child = this._children[i];
            let selectlist = child.querySelector( 'od-form > od-form-selectlist' );
            selectlist.appendChild( this._buildOption( title, title ) )
        }
    }

    _buildOption( value, title ) {
        let option = document.createElement( 'option' );
        option.setAttribute( 'value', value );
        option.innerHTML = title;
        return option;
    }

    async _optionChanged( e ) {
        let type = e.detail.data.optionType;
        let parent = e.detail.form.parentElement.parentElement;

        this._removeItem( this._getItem( parent ) );
        for( let i = 0; i < this._options.length; i++ ) {
            let option = this._options[i];
            if( option.title === type ) {                
                let container = parent.querySelector( '.item' );
                let element = await this._modeBuilder.buildRegItem( option.regItem, option.eventData, container );
                this._modeBuilder.configureFirstItem( element, option.regItem );
                break;
            }
        }
    }

    _getItem( container ) {
        let inner = container.querySelector( '.item' );
        return inner.firstChild;
    }

    _removeItem( item ) {
        if( !item ) {
            return;
        }
        let index = Number.parseInt( item.parentElement.parentElement.id );
        this._registrationInfo.childRegistrations.splice( index, 1 );

        item.remove();

        this._dispatchChange();
    }    

    _updateItemGroupRegistration( e ) {
        this._updateParentMultiOrItemGroupRegistration( e );
    }
    
    _updateMultiRegistration( e ) {
        this._updateParentMultiOrItemGroupRegistration( e );
    }

    _updateSingleVariableOrItem( e ) {
        let index = Number.parseInt( e.target.parentElement.parentElement.id );
        if( isNaN( index ) ) {
            return;
        }

        if( !e.detail.checked ) {
            this._registrationInfo.childRegistrations.splice( index, 1 );
            return;
        }

        let childItemRegistration = {
            'eventId': e.detail.eventId,
            'registrationId': e.detail.registrationId,
            'quantity': e.detail.quantity,
            'attendeeGroups': null,
            'childRegistrations': null
        }
        
        while( !this._registrationInfo.childRegistrations[index] ) {
            this._registrationInfo.childRegistrations.push( {} );
        }
        this._registrationInfo.childRegistrations[index] = childItemRegistration;

        this._dispatchChange();
    }

    _updateParentSingleRegistration( e ) {
        this._updateParentMultiOrItemGroupRegistration( e );
    }

    _updateParentMultiOrItemGroupRegistration( e ) {        
        let index = Number.parseInt( e.target.parentElement.parentElement.id );
        if( isNaN( index ) ) {
            return;
        }

        if( !e.detail.checked ) {
            this._registrationInfo.childRegistrations.splice( index, 1 );
            return;
        }

        let childItemRegistration = {
            'eventId': e.detail.eventId,
            'registrationId': e.detail.registrationId,
            'quantity': e.detail.quantity,
            'attendeeGroups': this._copy.copyArray( e.detail.registrationInfo.attendeeGroups ),
            'childRegistrations': this._copy.copyArray( e.detail.registrationInfo.childRegistrations )
        }        
        
        while( !this._registrationInfo.childRegistrations[index] ) {
            this._registrationInfo.childRegistrations.push( {} );
        }
        this._registrationInfo.childRegistrations[index] = childItemRegistration;
        this._dispatchChange();
    }

    _calculateTotals() {
        let cost = 0.00;
        let discount = 0.00;
        let quantity = 0;
        for( let i = 0; i < this._children.length; i++ ) {
            let item = this._children[i];
            let child = this._getItem( item );
            if( !child || !child.checked || child.checked === undefined ) {
                continue;
            }
            discount += child.totalDiscount;              
            cost += child.totalCost;
            quantity += 1;
        }
        this._quantity = Number.parseInt( quantity )
        this._total = Number.parseFloat( cost );
        this._discount = Number.parseFloat( discount );
    }

    _dispatchChange() {
        this.__dispatchRegInfoChange( 'parent-multi-registration-change' );
    }

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////

    validate() {
        let valid = true;
        let messgageSet = false;
        for( let i = 0; i < this._children.length; i++ ) {
            let child = this._children[i];
            let form = child.querySelector( '.form-container > od-form' );
            let item = child.querySelector( '.item > *' );
            if( !form.validate() ) {
                if( !messgageSet ) {
                    this._invalidMessage = this.title + ' must have have an option selected.';
                    messgageSet = true;
                }
                valid = false;
            }
            if( item && !item.validate() ) {
                if( !messgageSet ) {
                    this._invalidMessage = item.invalidMessage;
                    messgageSet = true;
                }
                valid = false;
            }
        }
        return valid;
    }
    
    createDefault() {
        let first = this._buildNewItem( this._internalCount );
        first.querySelector( 'hr' ).style.display = 'none';
        first.querySelector( 'button' ).style.display = 'none';
        this.appendChild( first );
    }

    async mutationPromise() {
        return new Promise( ( resolve ) => { 
            this.addEventListener( 'odp-event-multi-mutation', ( e ) => { this.mutationPromiseCallback( e, resolve ); } );
        } );
    }

    configureAddButton() {
        this._addButton.innerHTML = 'Add Another Option'
        this._addButton.onclick = ( e ) => {
            let element = this._buildNewItem( this._internalCount );
            this.appendChild( element );
        };
    }
}
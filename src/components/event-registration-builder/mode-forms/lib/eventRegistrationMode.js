import Copy from '../../../../lib/copy.js';
import Same from '../../../../lib/same.js';

export default class ODEventRegistrationMode extends HTMLElement {
    
    constructor( template ) {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {
        this._copy = new Copy();
        this._same = new Same();

        this._init();

        this._regItem = {};

        this._quantity = 1;
        this._discount = 0.00;
        this._total = 0.00;

        //set attribute default values first and pass to element
        this.setAttribute( 'show-checkbox', true );

        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( 'eventId' );
        this._upgradeProperty( 'registrationId' );
        this._upgradeProperty( 'regularPrice' );
        this._upgradeProperty( 'memberPrice' );
        this._upgradeProperty( 'title' );
        this._upgradeProperty( 'checked' );
        this._upgradeProperty( 'showCheckbox' );
        this._upgradeProperty( 'inline' );
        this._upgradeProperty( 'radioMode' );
        this._upgradeProperty( 'radioTitle' );

        this._upgradeProperty( 'eventData' );
        this._upgradeProperty( 'regItem' );
        this._upgradeProperty( 'totalCost' );
        this._upgradeProperty( 'totalDiscount' );

        //listeners and others etc.
        if( this._titleCheckbox ) {
            this._titleCheckbox.addEventListener( 'change', ( e ) => this.checked = e.target.checked );
        }

        this.dispatchEvent( new CustomEvent( 'add-member-hook', { bubbles: true, composed: true, 'detail': this } ) );
    }

    //General - every element should have this
    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return [
            'event-id',
            'registration-id',
            'regular-price',
            'member-price',
            'is-member',
            'show-checkbox',
            'checked',
            'title',
            'simple-form'
        ];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        switch ( attrName ) {
            case "inline": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setInlineAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setInlineAttribute( newValue );
                }
                break;
            }
            case "is-member": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setIsMemberAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setIsMemberAttribute( newValue );
                }
                break;
            }
            case "checked": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setCheckedAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setCheckedAttribute( newValue );
                }
                break;
            }
            case "show-checkbox": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setShowCheckboxAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setShowCheckboxAttribute( newValue );
                }
                break;
            }
            case "event-id": {
                if ( newValue !== null && newValue !== "" && !this._validateInt( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setEventIdAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setEventIdAttribute( newValue );
                }
                break;
            }
            case "registration-id": {
                if ( newValue !== null && newValue !== "" && !this._validateInt( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setRegistrationIdAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setRegistrationIdAttribute( newValue );
                }
                break;
            }
            case "regular-price": {
                if ( newValue !== null && newValue !== "" && !this._validateFloat( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setRegularPriceAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setRegularPriceAttribute( newValue );
                }
                break;
            }
            case "member-price": {
                if ( newValue !== null && newValue !== "" && !this._validateFloat( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setMemberPriceAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setMemberPriceAttribute( newValue );
                }
                break;
            }
            case "title": {
                if( newValue !== oldValue ) {
                    this._setTitleAttribute( newValue );
                }
                break;
            }
            case "simple-form": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setSimpleFormAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setSimpleFormAttribute( newValue );
                }
                break;
            }
        }
        this._dispatchChange();
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////

    get totalCost() {
        this._calculateTotals();
        return this._total;
    }

    get totalDiscount() {
        this._calculateTotals();
        return this._discount;
    }

    get eventData() {
        return this._eventData;
    }

    set eventData( eventData ) {
        this._eventData = eventData;
        this.eventId = eventData.id;
    }

    get regItem() {
        return this._regItem;
    }

    set regItem( regItem ) {
        this._regItem = regItem;
        this.memberPrice = regItem.memberPrice;
        this.regularPrice = regItem.regularPrice;
        this.eventId = regItem.eventId;
        this.registrationId = regItem.id;
    }

    _validateInt( newV ) {
        return (
            typeof newV === "number" || Number.parseInt( newV ) !== NaN
        );
    }

    _validateFloat( newV ) {
        return (
            typeof newV === "number" || Number.parseFloat( newV ) !== NaN
        );
    }
    
    _setNumberAttribute( name, newV ) {
        if ( newV === "" || newV === null ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, newV );
        }
    }

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }

    get eventId() {
        return Number.parseInt( this.getAttribute( "event-id" ) );
    }

    set eventId( id ) {
        if ( typeof id !== "number" ) {
            return;
        }
        this._setEventIdAttribute( id );
    }

    _setEventIdAttribute( id ) {        
        this._setNumberAttribute( 'event-id', id );
    }

    get registrationId() {
        return Number.parseInt( this.getAttribute( "registration-id" ) );
    }

    set registrationId( id ) {
        if ( typeof id !== "number" ) {
            return;
        }
        this._setRegistrationIdAttribute( id );
    }

    _setRegistrationIdAttribute( id ) {        
        this._setNumberAttribute( 'registration-id', id );
    }

    get regularPrice() {
        return Number.parseFloat( this.getAttribute( "regular-price" ) );
    }

    set regularPrice( price ) {
        if ( typeof price !== "number" && price !== null ) {
            return;
        }
        this._setRegularPriceAttribute( price );
    }

    _setRegularPriceAttribute( price ) {        
        this._setNumberAttribute( 'regular-price', price );
    }

    get memberPrice() {
        return Number.parseFloat( this.getAttribute( "member-price" ) );
    }

    set memberPrice( price ) {
        if ( typeof price !== "number"  && price !== null ) {
            return;
        }
        this._setMemberPriceAttribute( price );
    }

    _setMemberPriceAttribute( price ) {        
        this._setNumberAttribute( 'member-price', price );
    }

    get isMember() {
        return this.hasAttribute( "is-member" );
    }

    set isMember( isMember ) {
        if( typeof isMember !== 'boolean' ) {
            return;
        }
        this._setIsMemberAttribute( isMember );
    }

    _setIsMemberAttribute( isMember ) {
        this._setBooleanAttribute( 'is-member', isMember );
    }    

    get checked() {
        return this.hasAttribute( "checked" );
    }

    set checked( checked ) {
        if( typeof checked !== 'boolean' ) {
            return;
        }
        this._setCheckedAttribute( checked );
    }

    _setCheckedAttribute( checked ) {
        this._setBooleanAttribute( 'checked', checked );
        if( !this._titleCheckbox ) {
            return;
        }
        this._titleCheckbox.checked = this.checked;
    }

    get showCheckbox() {
        return this.hasAttribute( "show-checkbox" );
    }

    set showCheckbox( show ) {
        if( typeof show !== 'boolean' ) {
            return;
        }
        this._setShowCheckboxAttribute( show );
    }

    _setShowCheckboxAttribute( show ) {
        this._setBooleanAttribute( 'show-checkbox', show );
        if( !this._titleCheckbox ) {
            return;
        }
        if( !this.showCheckbox ) {            
            this._titleCheckbox.setAttribute( 'hide', true );
        } else {
            this._titleCheckbox.removeAttribute( 'hide' );
        }
    }

    get title() {
        return this.getAttribute( "title" );
    }

    set title( title ) {
        this._setTitleAttribute( title );
    }

    _setTitleAttribute( title ) {
        let temp = this._parseTitle( title )
        this._setTitleInDom( temp );

        if( temp === "" ) {
            this.removeAttribute( 'title' );
            return;
        }
        this.setAttribute( 'title', title )
    }

    get inline() {
        return this.hasAttribute( "inline" );
    }

    set inline( isInline ) {
        if ( typeof isInline !== "boolean" ) {
            return;
        }
        this._setInlineAttribute( isInline );
    }

    _setInlineAttribute( newV ) {
        this._setBooleanAttribute( 'inline', newV );
    } 
    
    get simpleForm() {
        return this.hasAttribute( "simple-form" );
    }

    set simpleForm( isSimple ) {
        if ( typeof isSimple !== "boolean" ) {
            return;
        }
        this._setSimpleFormAttribute( isSimple );
    }

    _setSimpleFormAttribute( newV ) {
        this._setBooleanAttribute( 'simple-form', newV );
    } 

    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////  
    
    _requestInlineHook() {
        this.dispatchEvent( new CustomEvent( 'add-inline-hook', { bubbles: true, composed: true, 'detail': this } ) );
    }

    _requestSimpleFormHook() {
        this.dispatchEvent( new CustomEvent( 'add-simple-form-hook', { bubbles: true, composed: true, 'detail': this } ) );
    }
    
    _parseTitle( title ) {
        if( !title ) {
            return "";
        }
        let newTitle = title;
        let breakChar = `
    `;
        let searchChar = ' - ';
        let current = 0;
        let breakIndex = newTitle.indexOf( searchChar );
        while( breakIndex > -1 ) {
            newTitle = newTitle.substring( 0, breakIndex ) + breakChar + newTitle.substring( breakIndex, newTitle.length );
            current = breakIndex + breakChar.length + 1;
            breakIndex = newTitle.indexOf( searchChar, current );
        }

        return newTitle;
    }

    _setTitleInDom( parsedTitle ) {
        if( this._titleCheckbox ) {
            this._titleCheckbox.value = parsedTitle;
        }
        if( this._titleLabel ) {
            this._titleLabel.innerHTML = parsedTitle;
        }
    }

    _calculateTotals() {
        let cost = 0.00;
        let discount = 0.00;
        if( this.isMember ) {
            cost = this.memberPrice;
            discount = this.regularPrice - this.memberPrice;
        } else {
            cost = this.regularPrice;
        }
        this._total = Number.parseInt( this._quantity ) * Number.parseFloat( cost );
        this._discount = Number.parseInt( this._quantity ) * Number.parseFloat( discount );
    }

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////

    getData() {
        return {};
    }

}
import Copy from '../../../../lib/copy.js';

export default class ModeBuilder {

    constructor( scope ) {
        this._scope = scope;
        this._copy = new Copy();

        this._nest = 0;
    }

    async buildRegItem( regItem, eventData, parent, mutationPromiseCallback ) {
        if( !regItem || regItem.registrationType === 0 ) {
            return;
        }
        let element = null;  
        switch( regItem.registrationType ) {
            case 1: {
                element = this._buildParentItemGroup();
                parent.appendChild( element );
                if( parent.mutationPromise ) {
                    await parent.mutationPromise();
                } else if( mutationPromiseCallback ){
                    await mutationPromiseCallback();
                }
                this._configureParentItemGroup( element, regItem, eventData );            
                await this._buildParentItemGroupChildren( element, eventData.childEvents );
                break;
            }
            case 2: {
                element = this._buildParentSingle();
                parent.appendChild( element );
                if( parent.mutationPromise ) {
                    await parent.mutationPromise();
                } else if( mutationPromiseCallback ){
                    await mutationPromiseCallback();
                }
                this._configureParentSingle( element, regItem, eventData );            
                await this._buildParentSingleChildren( element, eventData.childEvents );
                break;
            }
            case 3: {
                this._nest += 1;
                element = this._buildParentMulti( regItem );
                parent.appendChild( element );
                if( parent.mutationPromise ) {
                    await parent.mutationPromise();
                } else if( mutationPromiseCallback ){
                    await mutationPromiseCallback();
                }
                this._configureParentMulti( element, regItem, eventData );

                await this._buildParentMultiChildren( element, eventData.childEvents );
                
                element.configureAddButton();
                this._nest -= 1;
                break;
            }
            case 4: {
                element = this._buildSingleItem();
                parent.appendChild( element );
                if( parent.mutationPromise ) {
                    await parent.mutationPromise();
                } else if( mutationPromiseCallback ){
                    await mutationPromiseCallback();
                }
                this._configureSingleItem( element, regItem, eventData );
                break;
            }
            case 5: {
                element = this._buildSingleVariable( regItem );
                parent.appendChild( element );
                if( parent.mutationPromise ) {
                    await parent.mutationPromise();
                } else if( mutationPromiseCallback ){
                    await mutationPromiseCallback();
                }
                this._configureSingleVariable( element, regItem, eventData );
                break;
            }
            case 6: {
                element = this._buildParentItemGroup();
                parent.appendChild( element );
                if( parent.mutationPromise ) {
                    await parent.mutationPromise();
                } else if( mutationPromiseCallback ){
                    await mutationPromiseCallback();
                }
                this._configureParentItemGroup( element, regItem, eventData ); 
                let group = await this._buildAttendeeGroup( regItem, eventData, element, 1 );
                group.groupPrice = false;
                group.showTitle = false;
                element.configureAddButton( this, this._createAttendeeInGroup, regItem, eventData, group, 1, '+ Add Another Attendee' );
                break;
            }
            case 7: {
                element = await this._generateAttendeeGroup( regItem, eventData, parent, 2, '+ Add Another Attendee Group', mutationPromiseCallback );
                break;
            }
            case 8: {   
                element = await this._generateAttendeeGroup( regItem, eventData, parent, 3, '+ Add Another Attendee Group', mutationPromiseCallback );
                break;
            }
            case 9: {
                element = await this._generateAttendeeGroup( regItem, eventData, parent, 4, '+ Add Another Attendee Group', mutationPromiseCallback );
                break;
            }
            case 10: {
                element = this._buildParentItemGroup();
                parent.appendChild( element );
                if( parent.mutationPromise ) {
                    await parent.mutationPromise();
                } else if( mutationPromiseCallback ){
                    await mutationPromiseCallback();
                }
                this._configureParentItemGroup( element, regItem, eventData ); 
                let group = await this._buildAttendeeGroup( regItem, eventData, element, 1 );
                group.showTitle = false;
            }
            default: {
                break;
            }
        }
        return element;
    }

    configureFirstItem( element, regItem ) {
        if( !element || !regItem || regItem.registrationType === 0 ) {
            return;
        }
        
        element.checked = true;
        element.showCheckbox = false;

        switch( regItem.registrationType ) {
            case 1: {
                break;
            }
            case 2: {
                break;
            }
            case 3: {
                element.createDefault();
                break;
            }
            case 4: {
                break;
            }
            case 5: {
                break;
            }
            case 6: {
                element.attendeeMode = true;
                break;
            }
            case 7: {
                element.attendeeMode = true;
                break;
            }
            case 8: {
                element.attendeeMode = true;
                break;
            }
            case 9: {
                element.attendeeMode = true;
                break;
            }
            case 10: {
                element.attendeeMode = true;
            }
            default: {
                return;
            }
        }
    }

    // Parent Item Group ---------------------------------
    _buildParentItemGroup() {
        return document.createElement( 'odp-event-parent-item-group-registration' );
    }

    async _buildParentItemGroupChildren( itemGroup, childEvents ) {
        if( this._nest > 0 ) {
            return;
        }
        if( !childEvents ) {
            return;
        }
        for( let i = 0; i < childEvents.length; i++ ) {
            let event = childEvents[i];
            let regItem = event.registrationData[0];
            if( !regItem || regItem.registrationType === 0 ) {
                continue;
            }
            if( i !== 0 ) {
                itemGroup.appendChild( this._createDivider() );
                await itemGroup.mutationPromise();
            }
            let element = await this.buildRegItem( regItem, event, itemGroup );
            
            if( regItem.registrationType === 6 
                    || regItem.registrationType === 7
                    || regItem.registrationType === 8 
                    || regItem.registrationType === 9
                    || regItem.registrationType === 10  ) {
                element.attendeeMode = true;
            }            
        }
    }

    _configureParentItemGroup( item, regItem, eventData ) {        
        this._configureCommon( item, regItem, eventData );
        this._addRegistrationInfoHook( item );
    }

    _addRegistrationInfoHook( parentItemGroup ) {
        this._scope.shadowRoot.dispatchEvent( new CustomEvent(
            'add-registration-info-hook',
            {
                bubbles: true,
                composed: true,
                'detail': {
                    'parentItemGroup': parentItemGroup
                }
            } 
        ) );
    }

    // Parent Single -------------------------------------
    _buildParentSingle() {
        return document.createElement( 'odp-event-parent-single-registration' );
    }

    _configureParentSingle( item, regItem, eventData ) {        
        this._configureCommon( item, regItem, eventData );
    }

    async _buildParentSingleChildren( parentSingle, childEvents ){
        if( this._nest > 0 ) {
            return;
        }
        for( let i = 0; i < childEvents.length; i++ ) {
            let event = childEvents[i];
            let regItem = event.registrationData[0];
            if( !regItem || regItem.registrationType === 0 ) {
                continue;
            }
            let radio = this._createRadio( parentSingle.title, event.title );
            parentSingle.appendChild( radio );
            let item = await this.buildRegItem( regItem, event, radio );
            if( regItem.registrationType === 6 
                    || regItem.registrationType === 7
                    || regItem.registrationType === 8 
                    || regItem.registrationType === 9
                    || regItem.registrationType === 10  ) {
                item.attendeeMode = true;
            } 

            item.showCheckbox = false; 
            radio.firstChild.addEventListener( 'change', ( e ) => {
                let item = radio.firstChild.nextSibling;
                item.checked = true;
            } );    
        }
    }

    _createRadio( name, title ) {
        let container = document.createElement( 'div' );
        container.classList.add( 'radio-container' );
        container.style.display = 'flex';
        container.style.alignItems = 'center';
        container.style.margin = '1rem 0';

        let radio = document.createElement( 'input' );
        radio.setAttribute( 'part', 'mode-radio' );
        radio.setAttribute( 'type', 'radio' );
        radio.setAttribute( 'name', name );
        radio.value = title;
        container.appendChild( radio );
        return container;
    }

    // Parent Multi --------------------------------------
    _buildParentMulti() {
        return document.createElement( 'odp-event-parent-multi-registration' );
    }

    _configureParentMulti( item, regItem, eventData ) {        
        this._configureCommon( item, regItem, eventData );
    }
    
    async _buildParentMultiChildren( parentMulti, childEvents ){
        if( this._nest > 1 ) {
            return;
        }
        for( let i = 0; i < childEvents.length; i++ ) {
            let event = childEvents[i];
            let regItem = event.registrationData[0];
            if( !regItem || regItem.registrationType === 0 ) {
                continue;
            }
            await this.buildRegItem( regItem, event, parentMulti );         
        }
    }

    // Item ----------------------------------------------
    _buildSingleItem() {
        return document.createElement( 'odp-event-item-registration' );
    }

    _configureSingleItem( item, regItem, eventData ) {
        this._configureCommon( item, regItem, eventData );
    }

    //Variable ---------------------------------------
    _buildSingleVariable() {
        return document.createElement( 'odp-event-variable-registration' );
    }

    _configureSingleVariable( item, regItem, eventData ) {
        this._configureCommon( item, regItem, eventData );
    }

    //Single Attendee ------------------------------------
    async _createAttendeeInGroup( scope, regItem, eventData, groupElem, count ) {
        let attendee = document.createElement( 'odp-event-attendee-registration' );
        
        groupElem.appendChild( attendee );
        await groupElem.mutationPromise();
        
        scope._configureSingleAttendee( attendee, regItem, eventData );
        attendee.configureDeleteButton( scope._deleteAttenddeeInGroup, groupElem );

        return attendee;
    }

    _deleteAttenddeeInGroup( parent, remove ) {
        parent.removeChild( remove );
    }

    _configureSingleAttendee( item, regItem, eventData ) {        
        item.regItem = regItem;
        item.eventData = eventData;
        item.isMember = this._scope.isMember;
    }

    //Attendee Groups-----------------------------------
    async _generateAttendeeGroup( regItem, eventData, parent, count, buttonTitle, mutationPromiseCallback ) {        
        let element = this._buildParentItemGroup();
        parent.appendChild( element );
        if( parent.mutationPromise ) {
            await parent.mutationPromise();
        } else if( mutationPromiseCallback ) {
            await mutationPromiseCallback();
        }
        this._configureParentItemGroup( element, regItem, eventData );
        
        if( this._nest > 0 ) {
            return element;
        }
        await this._buildAttendeeGroup( regItem, eventData, element, count );
        
        element.configureAddButton( this, this._createAttendeeGroup, regItem, eventData, element, count, buttonTitle );
        return element;
    }

    async _buildAttendeeGroup( regItem, eventData, parent, count ) {        
        let group = await this._createAttendeeGroup( this, regItem, eventData, parent, count );
        group.showContactCheckbox = true;
        group.showDelete = false;
        group.showLine = false;
        return group;
    }

    async _createAttendeeGroup( scope, regItem, eventData, parent, count ) {
        let group = document.createElement( 'odp-event-attendee-group-registration' );

        parent.appendChild( group );
        await parent.mutationPromise();
        scope._configureAttendeeGroup( group, regItem, eventData );
        group.showCheckbox = false;
        group.checked = true;
        group.configureDeleteButton( scope._deleteGroup, parent );

        let element = null;
        for( let i = 0; i < count; i ++ ) {
            element = scope._createAttendee( i );
            group.appendChild( element );
            await group.mutationPromise();
            element.showDelete = false;
            if( i === 0 ) {
                element.showLine = false;
            }
            scope._configureSingleAttendee( element, regItem, eventData );
        }
        return group;
    }

    _createAttendee( index ) {
        let element = document.createElement( 'odp-event-attendee-registration' );
        element.index = index;
        return element;
    }

    _configureAttendeeGroup( item, regItem, eventData ) {
        this._configureCommon( item, regItem, eventData );
    }

    _deleteGroup( parent, remove ) {
        parent.removeChild( remove );
    }

    //Common -------------------------------
    _configureCommon( item, regItem, eventData ) {
        item.title = eventData.title;
        item.regItem = regItem;
        item.eventData = eventData;
        item.isMember = this._scope.isMember;
    }

    _createDivider() {
        let divider = document.createElement( 'hr' );
        divider.setAttribute( 'part', 'divider' );
        divider.style.display = 'flex';
        divider.style.width = '100%';
        return divider;
    }
}
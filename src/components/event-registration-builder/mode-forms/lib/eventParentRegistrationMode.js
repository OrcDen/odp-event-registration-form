import Copy from '../../../../lib/copy.js';
import Same from '../../../../lib/same.js';
import ODEventRegistrationMode from './eventRegistrationMode.js';

export default class ODEventParentRegistrationMode extends ODEventRegistrationMode {
    
    constructor( template ) {
        super( template );
    }

    connectedCallback() {
        this._registrationInfo = { 'attendeeGroups': [], 'childRegistrations': [] };
        this._storedData = {};

        this._copy = new Copy();
        this._same = new Same();
        
        super.connectedCallback();

        //set attribute default values first and pass to element

        //all properties should be upgraded to allow lazy functionality

        //listeners and others etc.
        if( this._itemsSlot ) {
            this._itemsSlot.addEventListener( 'parent-item-group-registration-change', ( e ) => {
                e.stopPropagation();
                this._updateItemGroupRegistration( e );
            } );
    
            this._itemsSlot.addEventListener( 'parent-multi-registration-change', ( e ) => {
                e.stopPropagation();
                this._updateMultiRegistration( e );
            } );
    
            this._itemsSlot.addEventListener( 'parent-single-registration-change', ( e ) => {
                e.stopPropagation();
                this._updateMultiRegistration( e );
            } );
    
            this._itemsSlot.addEventListener( 'item-registration-change', ( e ) => {
                e.stopPropagation();
                this._updateSingleVariableOrItem( e );
            } );
    
            this._itemsSlot.addEventListener( 'variable-registration-change', ( e ) => {
                e.stopPropagation();
                this._updateSingleVariableOrItem( e );
            } );
        }
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        super.attributeChangedCallback( attrName, oldValue, newValue );
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////

    
    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////
    
    _setRegistrationInfoIds() {
        this._registrationInfo['eventId'] = this.eventId;
        this._registrationInfo['registrationId'] = this.registrationId;
    }

    __dispatchRegInfoChange( eventName ) {
        let newData = this._copy.deepCopy( this.getData() );
        if( this._same.isSame( this._storedData, newData ) ) {
            return;
        }
        this._storedData = newData;
        this.shadowRoot.dispatchEvent( new CustomEvent(
            eventName,
            {
                bubbles: true,
                composed: true,
                'detail': this._storedData
            } 
        ) );
    }

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////

    getData() {
        this._calculateTotals();
        this._setRegistrationInfoIds();
        return {
            'registrationInfo': this._registrationInfo,
            'eventId': this.eventId,
            'registrationId': this.registrationId,
            'quantity': this._quantity,
            'discount': this.totalDiscount,
            'total': this.totalCost,
            'checked': this.checked
        }
    }

    configureAddButton( scope, callback, regItem, eventData, parent, count, title ) {
        this._addButton.onclick = async () => {
            await callback( scope, regItem, eventData, parent, count );
            this._dispatchChange();
        };
        this._addButton.innerHTML = title;
        this._addButton.style.display = 'block'
    }

}
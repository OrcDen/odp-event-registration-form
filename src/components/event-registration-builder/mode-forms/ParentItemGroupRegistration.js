import ODEventParentRegistrationMode from './lib/eventParentRegistrationMode.js';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        :host {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        #parent-item-group-registration {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        :host( [is-member] ) #reg-price {
            text-decoration: line-through;
        }

        :host( [is-member] ) #ag-reg-price {
            text-decoration: line-through;
        }

        :host( :not( [is-member] ) ) #mem-price {
            text-decoration: line-through;
        }

        :host( :not( [is-member] ) ) #ag-mem-price {
            text-decoration: line-through;
        }

        :host( :not( [show-all-option] ) ) #all-container {
            display: none;
        }

        :host( :not( [show-checkbox] ) ) #title-checkbox {
            display: none;
        }

        :host( :not( [show-checkbox] ) ) #item-container {
            border: none;
        }

        :host( [attendee-mode='true'] ) #all-container {
            display: none;
        }

        :host( :not( [attendee-mode] ) ) #attendee-group-prices {
            display: none;
        }

        :host( :not( [attendee-mode] ) ) #add-button {
            display: none;
        }

        :host( :not( [inline] ) ) .indent {
            padding-left: 0;
        }

        :host( :not( [inline] ) ) #parent-item-group-registration-inner {
            flex-direction: column;
            align-items: flex-start;
        }

        :host( :not( [inline] ) ) #item-container {
            padding-left: 1rem;
        }

        .price {
            white-space: pre;
        }

        .price[hide='true'] {
            display: none;
        }

        .price > span {
            font-weight: 600;
        }

        .container {
            display: flex;
            align-items: center;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .indent {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            margin-bottom: 1rem;
            padding-left: 2rem;
        }

        #parent-item-group-registration-inner {
            display: flex;
            align-items: center;
        }

        #prices {
            display: flex;
            flex-direction: column;

            margin-left: 4rem;
        }

        #attendee-group-prices {
            display: flex;
            flex-direction: column;

            margin-left: 4rem;
        }

        #title-container {
            display: flex;
            align-items: center;
        }
    
        #title {
            font-weight: 600;
            font-size: 1.25rem;
            margin: 0 1rem;
        }

        #all-label {
            white-space: pre;
            font-weight: 600;
            font-size: 1.1rem
        }

        #all-container {
            margin: 1rem 0 2rem 0;
        }

        #item-container {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding-left: 2rem;
            border-left: 1px solid darkgrey;
        }

        #qty {
            white-space: pre;
            margin-left: 2rem;
        }

        #qty-input {
            width: 2rem;
        }

        #add-button {
            display: none;
            margin-top: 1rem;
        }

    </style>
    
    <div id="parent-item-group-registration">
        <div id='parent-item-group-registration-inner'>
            <div id='title-container'>
                <input id='title-checkbox' type='checkbox' part='mode-checkbox'>
                <label id='title' part='mode-item-group-title'></label>
            </div>
            <div id='attendee-group-prices'>
                <label id='ag-reg-price' class='price' part='mode-price'>Regular Price: $<span></span></label>
                <label id='ag-mem-price' class='price' part='mode-price'>Member Price: $<span></span></label>
                <label id='ag-free-price' class='price' part='mode-price'>FREE</label>
            </div>
        </div>
        <div class='indent'>
            <div class='container' id='all-container' >
                <div class='container'>
                    <input id='all-checkbox' type='checkbox' part='mode-checkbox'>
                    <label id='all-label' part='mode-item-title'> All</label>
                </div>
                <div id='prices'>
                    <label id='reg-price' class='price' part='mode-price'>Regular Price: $<span></span></label>
                    <label id='mem-price' class='price' part='mode-price'>Member Price: $<span></span></label>
                    <label id='free-price' class='price' part='mode-price'>FREE</label>
                </div>
                <label id='qty' part='mode-qty-title'>qty: </label>
                <input id='qty-input' type='number' value=1 part='mode-qty-input'>
            </div>
            <div id='item-container'>
                <slot id='items'></slot>
                <button id='add-button' part='mode-add-button'></button>
            </div>        
        </div>
    </div>         
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "odp-event-parent-item-group-registration" );

export class OdpEventParentItemGroupRegistration extends ODEventParentRegistrationMode {
    constructor() {
        super( template );
    }

    _init() {
        this._storedContactInfo =  {};
        this._storedAttendeeInfo =  {};
        this._hasSameAsContact = false;
        this._children = [];
        this._allQuantity = 1;

        this._titleLabel = this.shadowRoot.querySelector( '#title' );
        this._titleCheckbox = this.shadowRoot.querySelector( '#title-checkbox' );

        this._regularPriceLabel = this.shadowRoot.querySelector( '#reg-price' );
        this._regularPriceSpan = this._regularPriceLabel.querySelector( 'span' );
        this._agRegularPriceLabel = this.shadowRoot.querySelector( '#ag-reg-price' );
        this._agRegularPriceSpan = this._agRegularPriceLabel.querySelector( 'span' );

        this._memberPriceLabel = this.shadowRoot.querySelector( '#mem-price' );
        this._memberPriceSpan = this._memberPriceLabel.querySelector( 'span' );
        this._agMemberPriceLabel = this.shadowRoot.querySelector( '#ag-mem-price' );
        this._agMemberPriceSpan = this._agMemberPriceLabel.querySelector( 'span' );


        this._freeLabel = this.shadowRoot.querySelector( '#free-price' );
        this._agFreeLabel = this.shadowRoot.querySelector( '#ag-free-price' );

        this._allCheckbox = this.shadowRoot.querySelector( '#all-checkbox' );
        this._qtyInput = this.shadowRoot.querySelector( '#qty-input' );
        this._itemsSlot = this.shadowRoot.querySelector( '#items' );
        this._addButton = this.shadowRoot.querySelector( '#add-button' );

        this._invalidMessage = "";
    }

    connectedCallback() {
        super.connectedCallback();        

        //set attribute default values first and pass to element
        this.setAttribute( 'show-all-option', true );

        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( 'showAllOption' );
        this._upgradeProperty( 'attendeeMode' );

        //listeners and others etc.
        let config = { attributes: false, childList: true, subtree: true };
        this._observer = new MutationObserver( ( mutationsList, observer ) => {        
            for( let i = 0; i < mutationsList.length; i++ ) {
                let record = mutationsList[i];
                if( record.addedNodes && record.addedNodes.length ) {
                    for( let j = 0; j < record.addedNodes.length; j++ ) {
                        let node = record.addedNodes[j];
                        if( node.parentElement !== this ) {
                            continue;
                        }
                        if( node.tagName.includes( 'ODP-EVENT') ) {
                            this._children.push( node );
                        }
                    }
                }
                if( record.removedNodes && record.removedNodes.length ) {
                    for( let j = 0; j < record.removedNodes.length; j++ ) {
                        let node = record.removedNodes[j];
                        if( node.tagName.includes( 'ODP-EVENT') ) {                               
                            for( let k = 0; k < this._children.length; k++ ) {
                                if( node === this._children[k] ) {
                                    this._children.splice( k, 1 );
                                    this._updateAttendeeModeGroupRegistrations();
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            this.reIndexGroups();
            this._checkAllOption();
            this._mutationObserverCallback( mutationsList, observer );  
            this._dispatchChange();  
        } );
        this._observer.observe( this , config );        

        this._itemsSlot.addEventListener( 'attendee-group-registration-change', ( e ) => {
            e.stopPropagation();
            this._updateAttendeeGroupRegistrations( e ); 
        } );

        this._itemsSlot.addEventListener( 'group-has-same-change', ( e ) => {
            e.stopPropagation();
            this._hasSameListener( e ); 
        } );

        this._allCheckbox.addEventListener( 'change', ( e ) => {
            this._updateRegInfoAll( e.target.checked, this._allQuantity );
            if( e.target.checked && this._allQuantity === 0 ) {                
                this._updateQuantity( 1 );
            }
            this._dispatchChange();
        } );
        this._qtyInput.addEventListener( 'change', ( e ) => {            
            this._updateQuantity( e.target.value );
        } );
        
        this._requestInlineHook();
        
        this.setAttribute( 'exportparts', 'mode-radio, mode-item-group-title, mode-add-button, mode-qty-input, mode-qty-title, mode-item-title, mode-price, mode-attendee-group-title, mode-attendee-title, divider, mode-checkbox, mode-delete-button, mode-attendee-form, mode-attendee-table, mode-attendee-table-row, mode-attendee-table-cell, mode-attendee-label-cell, mode-attendee-input-cell, mode-attendee-field-label, mode-attendee-field-label-row, mode-attendee-value-label, mode-attendee-form-input, mode-attendee-form-select, mode-attendee-form-textarea, mode-attendee-form-richtext, mode-attendee-richtext-editor, mode-multi-form, mode-multi-table, mode-multi-table-row, mode-multi-table-cell, mode-multi-label-cell, mode-multi-input-cell, mode-multi-field-label, mode-multi-field-label-row, mode-multi-value-label, mode-multi-form-input, mode-multi-form-select, mode-multi-form-textarea, mode-multi-form-richtext, mode-multi-richtext-editor' );

        this._dispatchChange();
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return [
            'event-id',
            'registration-id',
            'regular-price',
            'member-price',
            'is-member',
            'show-checkbox',
            'checked',
            'title',
            'inline',
            'show-all-option',
            'attendee-mode'
        ];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        super.attributeChangedCallback( attrName, oldValue, newValue );
        switch ( attrName ) {
            case "show-all-option": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setShowAllOptionAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setShowAllOptionAttribute( newValue );
                }
                break;
            }
            case "attendee-mode": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setAttendeeModeAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setAttendeeModeAttribute( newValue );
                }
                break;
            }
        }
        this._dispatchChange();
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////

    get invalidMessage() {
        return this._invalidMessage;
    }
    
    _setRegularPriceAttribute( price ) {        
        this._setNumberAttribute( 'regular-price', price );
        this._setRegularPriceInDom( this.regularPrice );
    }

    _setMemberPriceAttribute( price ) {        
        this._setNumberAttribute( 'member-price', price );
        this._setMemberPriceInDom( this.memberPrice );
    }

    get showAllOption() {
        return this.hasAttribute( "show-all-option" );
    }

    set showAllOption( show ) {
        if( typeof show !== 'boolean' ) {
            return;
        }
        this._setShowAllOptionAttribute( show );
    }

    _setShowAllOptionAttribute( show ) {
        this._setBooleanAttribute( 'show-all-option', show );
        if( this.showAllOption ) {
            this._allQuantity = Number.parseInt( this._qtyInput.value );
        } else {
            this._allQuantity = 1;
        }
        this._updateRegInfoAll( this._allCheckbox.checked, this._allQuantity );
        this._dispatchChange();
    }

    get attendeeMode() {
        return this.hasAttribute( "attendee-mode" );
    }

    set attendeeMode( isAttendeeMode ) {
        if( typeof isAttendeeMode !== 'boolean' ) {
            return;
        }
        this._setAttendeeModeAttribute( isAttendeeMode );
    }

    _setAttendeeModeAttribute( isAttendeeMode ) {
        this._setBooleanAttribute( 'attendee-mode', isAttendeeMode );
    }

    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    
    _mutationObserverCallback( mutationsList, observer ) {
        this.dispatchEvent(
            new CustomEvent( 'odp-event-item-group-mutation', { 
                bubbles: true, 
                detail: { 
                    'mutationsList': mutationsList,
                    'observer': observer
                }
            } )
        );
    }

    mutationPromiseCallback( e, resolve ) {
        e.stopPropagation();
        this.removeEventListener( 'odp-event-item-group-mutation', ( e ) => { this.mutationPromiseCallback( e, resolve ); } );
        resolve( e.detail ); 
    }    

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////


    _setRegularPriceInDom( price ) {
        if( price === null || isNaN( price )  ) {
            this._regularPriceLabel.setAttribute( 'hide', true );
            this._agRegularPriceLabel.setAttribute( 'hide', true );
        } else if( price === 0 ) {
            this._regularPriceLabel.removeAttribute( 'hide' );
            this._regularPriceSpan.innerHTML = "FREE"
            this._agRegularPriceLabel.removeAttribute( 'hide' );
            this._agRegularPriceSpan.innerHTML = 'FREE';
        } else {
            this._regularPriceLabel.removeAttribute( 'hide' );
            this._regularPriceSpan.innerHTML = price.toFixed(2);
            this._agRegularPriceLabel.removeAttribute( 'hide' );
            this._agRegularPriceSpan.innerHTML = price.toFixed(2);
        }
        this._checkFree();
    }

    _setMemberPriceInDom( price ) {
        if( price === null || isNaN( price ) ) {
            this._memberPriceLabel.setAttribute( 'hide', true );
            this._agMemberPriceLabel.setAttribute( 'hide', true );
        } else if( price === 0 ) {
            this._memberPriceLabel.removeAttribute( 'hide' );
            this._memberPriceSpan.innerHTML = "FREE"
            this._agMemberPriceLabel.removeAttribute( 'hide' );
            this._agMemberPriceSpan.innerHTML = 'FREE';
        } else {
            this._memberPriceLabel.removeAttribute( 'hide' );
            this._memberPriceSpan.innerHTML = price.toFixed(2);
            this._agMemberPriceLabel.removeAttribute( 'hide' );
            this._agMemberPriceSpan.innerHTML = price.toFixed(2);
        }
        this._checkFree();
    }

    _checkFree() {
        this._freeLabel.setAttribute( 'hide', true );
        this._agFreeLabel.setAttribute( 'hide', true );
        if( this.memberPrice === 0 && this.regularPrice === 0 ) {
            this._regularPriceLabel.setAttribute( 'hide', true );            
            this._memberPriceLabel.setAttribute( 'hide', true );
        } 
        if( this._regularPriceLabel.hasAttribute( 'hide' ) && this._memberPriceLabel.hasAttribute( 'hide' ) ) {
            this._freeLabel.setAttribute( 'hide', false );
            this._agFreeLabel.setAttribute( 'hide', false );
        }
        this._checkAllOption();
    }

    _checkAllOption() {
        let showAll = true;
        for( let i = 0; i < this._children.length; i++ ) {
            let child = this._children[i];
            if( child.tagName !== 'ODP-EVENT-ITEM-REGISTRATION' ) {
                showAll = false;
            }
        }
        if( isNaN( this.regularPrice ) && isNaN( this.memberPrice ) ) {
            this.showAllOption = false;
        } else {
            this.showAllOption = showAll
        }
    }

    _updateSingleVariableOrItem( e ) {
        let childItemRegistration = {
            'eventId': e.detail.eventId,
            'registrationId': e.detail.registrationId,
            'quantity': e.detail.quantity,
            'attendeeGroups': null,
            'childRegistrations': null
        }
        this._updateRegistrationInfo( childItemRegistration, e.detail.checked );
    }

    _updateItemGroupRegistration( e ) {
        let newRegistration = this._copy.deepCopy( e.detail.registrationInfo );
        this._updateRegistrationInfo( newRegistration, e.detail.checked );
    }

    _updateMultiRegistration( e ) {
        let newRegistration = this._copy.deepCopy( e.detail.registrationInfo )
        this._updateRegistrationInfo( newRegistration, e.detail.checked );
    }

    _updateParentSingleRegistration( e ) {
        let newRegistration = this._copy.deepCopy( e.detail.registrationInfo )
        this._updateRegistrationInfo( newRegistration, e.detail.checked );
    }

    _updateAttendeeGroupRegistrations( e ) {
        if( isNaN( e.detail.index ) ) {
            return;
        }

        let eventId = e.detail.eventId;
        let regId = e.detail.registrationId;
        let groupInfo = this._copy.deepCopy( e.detail.attendeeGroupInfo );

        if( eventId === this.eventId && regId == this.registrationId ) {
            while( !this._registrationInfo.attendeeGroups[e.detail.index] ) {
                this._registrationInfo.attendeeGroups.push( {} );
            }
            this._registrationInfo.attendeeGroups[e.detail.index] = groupInfo;
            if( !this._hasSameAsContact && this._registrationInfo.attendeeGroups[0].attendees[0] ) {
                this._saveAttendeeData( this._registrationInfo.attendeeGroups[0].attendees[0] );
            }
            if( groupInfo.attendees ) {
                for( let i = 0; i < groupInfo.attendees.length; i++ ) {
                    if( Object.keys( groupInfo.attendees[i] ).length > 0 ) {
                        this.checked = true;
                    }
                }
            }
            this._dispatchChange();
        }
    }

    _hasSameListener( e ) {
        e.stopPropagation();
        this._hasSameAsContact = e.detail.hasSameAsContact;
        if( !this._hasSameAsContact ) {
            this._loadAttendeeData( this._storedAttendeeInfo )
        } else {
            this._loadAttendeeData( this._storedContactInfo );
        }
        this._dispatchChange();
    }
    
    _saveAttendeeData( data ) {
        if( data ) {
            this._storedAttendeeInfo = this._copy.deepCopy( data );
        }
    }

    _loadAttendeeData( data ) {
        if( this._children[0] && data ) {
            this._children[0]._setFirstAttendee( data )
        }
    }

    _updateAttendeeModeGroupRegistrations() {
        if( !this.attendeeMode ) {
            return;
        }
        this._registrationInfo.attendeeGroups = [];
        for( let i = 0; i < this._children.length; i++ ) {
            let child = this._children[i];
            this._registrationInfo.attendeeGroups.push( child.getData().attendeeGroupInfo );
        }
        this._dispatchChange();
    }

    _updateQuantity( qty ) {
        this._qtyInput.value = qty;
        this._allQuantity = Number.parseInt( qty );
        this._allCheckbox.checked = this._allQuantity > 0;            
        this._updateRegInfoAll( this._allCheckbox.checked, this._allQuantity );
        this._dispatchChange();
    }
    
    _updateRegInfoAll( allChecked, quantity ) {
        let allRegistration = {
            'eventId': this.eventId,
            'registrationId': this.registrationId,
            'quantity': quantity,
            'attendeeGroups': null,
            'childRegistrations': null
        };
        this._updateRegistrationInfo( allRegistration, allChecked )
    }

    _updateRegistrationInfo( registration, add ) {
        if( add ) {
            this.checked = true;
        }
        let found = false;
        for( let i = 0; i < this._registrationInfo.childRegistrations.length; i++ ) {
            let regInfo = this._registrationInfo.childRegistrations[i];
            if( regInfo.eventId === registration.eventId && regInfo.registrationId === registration.registrationId ) {
                if( !add ) {
                    this._registrationInfo.childRegistrations.splice( i, 1 );
                } else {
                    this._registrationInfo.childRegistrations[i] = registration;
                }
                found = true;
            }
        }
        if( add && !found ) {
            this._registrationInfo.childRegistrations.push( registration );
        }
        this._dispatchChange();
    }

    _calculateTotals() {
        let cost = 0.00;
        let discount = 0.00;

        if ( this.attendeeMode ) {
            for( let i = 0; i < this._children.length; i++ ) {
                let child = this._children[i];
                cost += child.totalCost;
                if( child.isMember ) {
                    discount += child.totalDiscount;
                }
            }
        } else {
            if( this._allCheckbox.checked ) {
                if( this.isMember ) {
                    cost += this.memberPrice;
                    discount += this.regularPrice - this.memberPrice;
                } else {
                    cost += this.regularPrice;
                }
                cost = cost * this._allQuantity;
                discount = discount * this._allQuantity;
            }
            for( let i = 0; i < this._children.length; i++ ) {
                let child = this._children[i];
                if( !child.checked || child.checked === undefined ) {
                    continue;
                }
                discount += child.totalDiscount;              
                cost += child.totalCost;
            }
        }    
    
        this._total = Number.parseInt( this._quantity ) * Number.parseFloat( cost );
        this._discount = Number.parseInt( this._quantity ) * Number.parseFloat( discount );
    }

    _contactInfoChangedHook( e ) {
        let contactInfo =  this._copy.deepCopy( e.detail.contactInfo );
        this._storedContactInfo = {
            'firstName': contactInfo['contactFirstName'],
            'lastName': contactInfo['contactLastName'],
            'email': contactInfo['contactEmail'],
            'phone': contactInfo['contactPhone'],
            'companyName': contactInfo['companyName'],
            'title': contactInfo['contactTitle']
        }
        if( this._hasSameAsContact ) {
            this._loadAttendeeData( this._storedContactInfo );
            this._dispatchChange();
        }
    }
    
    _setFirstAttendee() {
        if( this.attendeeMode ) {
            if( this._hasSameAsContact ) {
                this._registrationInfo.attendeeGroups[0].attendees[0] = this._storedContactInfo;
            } else {
                this._registrationInfo.attendeeGroups[0].attendees[0] = this._storedAttendeeInfo;
            }
        }
    }

    _dispatchChange() {
        this._setFirstAttendee();
        this.__dispatchRegInfoChange( 'parent-item-group-registration-change' );
    }

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////

    validate() {
        let valid = true;
        let oneChecked = false;
        let messgageSet = false;
        for( let i = 0; i < this._children.length; i++ ) {
            let child = this._children[i];
            if( child.checked ) {
                oneChecked = true;
                if( !child.validate() ) {
                    if( !messgageSet ) {
                        this._invalidMessage = child.invalidMessage;
                        messgageSet = true;
                    }
                    valid = false;
                }
            }
        }
        if( !oneChecked ) {
            valid = false;
            this._invalidMessage = this.title + ' must have at least one option selected.'
        }
        return valid;
    }

    async mutationPromise() {
        return new Promise( ( resolve ) => { 
            this.addEventListener( 'odp-event-item-group-mutation', ( e ) => { this.mutationPromiseCallback( e, resolve ); } );
        } );
    }

    reIndexGroups() {
        if( this.attendeeMode ) {
            for( let i = 0; i < this._children.length; i++ ) {
                this._children[i].index = i;
            }
        }
    }
}
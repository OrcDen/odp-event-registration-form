import '@orcden/od-forms';
import ODEventRegistrationMode from './lib/eventRegistrationMode.js';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        :host {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        #attendee-registration {
            display: flex;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        
        :host( :not( [inline] ) ) #attendee-registration-inner {
            flex-direction: column;
            align-items: flex-start;
        }

        :host( :not( [show-contact-checkbox='true'] ) ) #checkbox-container {
            display: none;
        }

        :host( [show-contact-checkbox='true'] ) #divider {
            display: none;
        }

        :host( :not( [show-line] ) ) #divider {
            display: none;
        }

        :host( :not( [show-delete] ) ) .delete-button {
            display: none;
        }

        #attendee-registration-inner {
            display: flex;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            align-items: center;
        }
    
        #attendee-title {
            font-weight: 600;
            white-space: nowrap;
        }

        #container {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        hr {
            width: 100%;
        }

        .delete-button::after {
            display: block;
            content: 'X';
        }

        #form {
            width: 100%;
        }

    </style>
    
    <hr id='divider' part='divider'>
    <div id="attendee-registration">
        <div id='attendee-registration-inner'>
            <label id='attendee-title' part='mode-attendee-title'>Attendee <span></span>:</label>
            <div id='container'>
                <div id='checkbox-container'>
                    <input
                        id='same-checkbox'
                        type='checkbox'
                        name='sameAsContact'
                        part='mode-checkbox'>: Same As Contact</input>
                </div>
                <od-form id='form'>            
                    <od-form-input
                        name='firstName'>First Name: </od-form-input>
                    <od-form-input
                        name='lastName'>Last Name: </od-form-input>
                    <od-form-input
                        name='email'
                        type='email'>Email: </od-form-input>
                    <od-form-input
                        name='phone'
                        type='tel'>Phone: </od-form-input>
                    <od-form-input
                        name='companyName'>Company: </od-form-input>
                    <od-form-input
                        name='title'>Job Title: </od-form-input>
                </od-form>
            </div>
        </div>        
        <button class='delete-button' part='mode-delete-button' ></button>
    </div>         
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "odp-event-attendee-registration" );

export class OdpEventAttendeeRegistration extends ODEventRegistrationMode {
    constructor() {
        super( template );
    }

    _init() {
        this._attendeeInfo = {};
        this._isSame = false;

        this._sameCheckbox = this.shadowRoot.querySelector( '#same-checkbox' );

        this._form = this.shadowRoot.querySelector( '#form' );
        this._firstNameInput = this.shadowRoot.querySelector( 'od-form-input[name="firstName"]' );
        this._lastNameInput = this.shadowRoot.querySelector( 'od-form-input[name="lastName"]' );
        this._emailInput = this.shadowRoot.querySelector( 'od-form-input[name="email"]' );
        this._phoneInput = this.shadowRoot.querySelector( 'od-form-input[name="phone"]' );
        this._companyNameInput = this.shadowRoot.querySelector( 'od-form-input[name="companyName"]' );
        this._titleInput = this.shadowRoot.querySelector( 'od-form-input[name="title"]' );

        this._deleteButton = this.shadowRoot.querySelector( '.delete-button' );
    }

    connectedCallback() {
        super.connectedCallback();      

        //set attribute default values first and pass to element
        this.setAttribute( 'show-delete', true );
        this.setAttribute( 'show-line', true );
        this.setAttribute( 'checked', true );
        if( !this.hasAttribute( 'index' ) ) {
            this.setAttribute( 'index', 0 );
        }

        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( 'index' );
        this._upgradeProperty( 'showDelete' );
        this._upgradeProperty( 'showLine' );

        //listeners and others etc.
        this._sameCheckbox.addEventListener( 'change', ( e ) => { 
            this._toggleSame( e ); 
        } );
        
        this._form.addEventListener( 'od-form-ready', () => { // wait for the form to  be ready
            this._form.form.addEventListener( 'submit', ( e ) => { 
                e.stopPropagation();
                e.preventDefault();
                this.validate();
                return false;
            } );
            this._form.form.addEventListener( 'change', ( e ) => {
                this._attendeeInfo = this._form.getData();
                this._dispatchChange();
            } );
        } );

        this._requestInlineHook();
        this._requestSimpleFormHook();

        this._form.setAttribute( 'exportparts', 'form, table, table-row, table-cell, label-cell, input-cell, field-label, field-label-row, value-label, form-input, form-select, form-textarea, form-richtext, richtext-editor' );
        this.setAttribute( 'exportparts', 'mode-attendee-title, divider, mode-checkbox, mode-delete-button, form : mode-attendee-form, table : mode-attendee-table, table-row : mode-attendee-table-row, table-cell : mode-attendee-table-cell, label-cell : mode-attendee-label-cell, input-cell : mode-attendee-input-cell, field-label : mode-attendee-field-label, field-label-row : mode-attendee-field-label-row, value-label : mode-attendee-value-label, form-input : mode-attendee-form-input, form-select : mode-attendee-form-select, form-textarea : mode-attendee-form-textarea, form-richtext : mode-attendee-form-richtext, richtext-editor : mode-attendee-richtext-editor' );
    
        this._dispatchChange();
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return [
            'event-id',
            'registration-id',
            'regular-price',
            'member-price',
            'is-member',
            'inline',
            'index',
            'show-delete',
            'show-line',
            'simple-form'
        ];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        super.attributeChangedCallback( attrName, oldValue, newValue );
        switch ( attrName ) {
            case "index": {
                if ( newValue !== null && newValue !== "" && !this._validateInt( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setIndexAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setIndexAttribute( newValue );
                }
                break;
            }
            case "show-delete": {
                if ( newValue !== null && newValue !== "" && !this._validateInt( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setShowDeleteAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setShowDeleteAttribute( newValue );
                }
                break;
            }
            case "show-line": {
                if ( newValue !== null && newValue !== "" && !this._validateInt( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setShowLineAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setShowLineAttribute( newValue );
                }
                break;
            }
        }
        this._dispatchChange();
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////

    
    get index() {
        return Number.parseInt( this.getAttribute( "index" ) );
    }

    set index( index ) {
        if ( typeof index !== "number" ) {
            return;
        }
        this._setIndexAttribute( index );
    }

    _setIndexAttribute( index ) {        
        this._setNumberAttribute( 'index', index );
        this.shadowRoot.querySelector( '#attendee-title > span' ).innerHTML = index + 1;
    }

    get showDelete() {
        return this.hasAttribute( 'show-delete' );
    }

    set showDelete( show ) {
        if( typeof show !== 'boolean' ) {
            return;
        }
        this._setShowDeleteAttribute( show );
    }

    _setShowDeleteAttribute( show ) {
        this._setBooleanAttribute( 'show-delete', show );
    }

    get showLine() {
        return this.hasAttribute( 'show-line' );
    }

    set showLine( show ) {
        if( typeof show !== 'boolean' ) {
            return;
        }
        this._setShowLineAttribute( show );
    }

    _setShowLineAttribute( show ) {
        this._setBooleanAttribute( 'show-line', show );
    }

    _setInlineAttribute( newV ) {
        this._setBooleanAttribute( 'inline', newV );
        
        this._setInlineInputs();
    } 

    _setSimpleFormAttribute( newV ) {
        this._setBooleanAttribute( 'simple-form', newV );
        
        this._toggleSimpleForm( this.simpleForm );
    } 

    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////

    _toggleSame( e ) {
        this._isSame = e.target.checked;
        if( !this._isSame ) {
            this._firstNameInput.removeAttribute( 'readonly' );
            this._lastNameInput.removeAttribute( 'readonly' );
            this._emailInput.removeAttribute( 'readonly' );
            this._phoneInput.removeAttribute( 'readonly' );
            this._companyNameInput.removeAttribute( 'readonly' );
            this._titleInput.removeAttribute( 'readonly' );
        } else {
            this._firstNameInput.setAttribute( 'readonly', true );
            this._lastNameInput.setAttribute( 'readonly', true );
            this._emailInput.setAttribute( 'readonly', true );
            this._phoneInput.setAttribute( 'readonly', true );
            this._companyNameInput.setAttribute( 'readonly', true );
            this._titleInput.setAttribute( 'readonly', true );
        }      

        this._dispatchIsSame();
    }

    _setInlineInputs() {
        this._firstNameInput.inline = this.inline;
        this._lastNameInput.inline = this.inline;
        this._emailInput.inline = this.inline;
        this._phoneInput.inline = this.inline;
        this._companyNameInput.inline = this.inline;
        this._titleInput.inline = this.inline;
    }

    _dispatchIsSame() {
        this.shadowRoot.dispatchEvent( new CustomEvent(
            'attendee-is-same-change',
            {
                bubbles: true,
                composed: true,
                'detail': {
                    'isSameAsContact': this._isSame,
                }
            } 
        ) );
    }

    _dispatchChange() {        
        this.shadowRoot.dispatchEvent( new CustomEvent(
            'attendee-registration-change',
            {
                bubbles: true,
                composed: true,
                'detail': this.getData()
            } 
        ) );
    }

    _toggleSimpleForm( isSimple ) {
        
        if ( !this._form.form || !this._form.form.querySelector('input[name="lastName"]')) {
            requestAnimationFrame(()=> this._toggleSimpleForm(isSimple));
            return;
        }
        if( isSimple ) {
            this._firstNameInput.innerHTML = "First & Last Name:";
            this._form.form.querySelector('input[name="lastName"]').parentElement.parentElement.style.display = 'none';
            this._form.form.querySelector('input[name="phone"]').parentElement.parentElement.style.display = 'none';
            this._form.form.querySelector('input[name="companyName"]').parentElement.parentElement.style.display = 'none';
            this._form.form.querySelector('input[name="title"]').parentElement.parentElement.style.display = 'none';
        } else {
            this._firstNameInput.innerHTML = "First Name:";
            this._form.form.querySelector('input[name="lastName"]').parentElement.parentElement.style.display = undefined;
            this._form.form.querySelector('input[name="phone"]').parentElement.parentElement.style.display = undefined;
            this._form.form.querySelector('input[name="companyName"]').parentElement.parentElement.style.display = undefined;
            this._form.form.querySelector('input[name="title"]').parentElement.parentElement.style.display = undefined;
            //this._lastNameInput.input.previousElementSibling.style.display = 'unset';
        }
    }

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////

    validate() {
        if( !this._form.validate() ) {
            this._form.htmlValidate();
            return false;
        }
        return true;
    }

    setData( attendee ) {
        this._form.populate( attendee );
        this._attendeeInfo = attendee;
        this._dispatchChange();
    }

    getData() {
        this._calculateTotals();
        return {
            'eventId': this.eventId,
            'registrationId': this.registrationId,
            'quantity': 1,
            'discount': this.totalDiscount,
            'total': this.totalCost,
            'attendeeInfo': this._attendeeInfo,
            'isMember': this.isMember,
            'index': this.index,
            'type': 6,
        }
    }

    configureDeleteButton( callback, parent ) {
        this._deleteButton.onclick = async () => {
            await callback( parent, this );
        };
        this.showDelete = true;
    }
    
    showSameCheckbox() {
        this._sameCheckbox.removeAttribute( 'hidden' );
    }
}
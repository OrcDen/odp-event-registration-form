import ODEventRegistrationMode from './lib/eventRegistrationMode.js';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        :host {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        #item-registration {
            display: flex;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            align-items: center;
        }

        :host( :not( [show-checkbox] ) ) #title-checkbox {
            display: none;
        }

        :host( :not( [inline] ) ) #item-registration {
            flex-direction: column;
            align-items: flex-start;
        }

        #title-container {
            display: flex;
            align-items: center;
        }
    
        #title {
            margin: 0 1rem;
            font-weight: 600;
            font-size: 1.1rem;
        }

        #qty {
            white-space: pre;
            margin-left: 2rem;
        }

        #qty-input {
            width: 2rem;
        }

        #prices {
            display: flex;
            flex-direction: column;

            margin-left: 2rem;
        }

        #qty-container {
            display: flex;
        }

        :host( [is-member] ) #reg-price {
            text-decoration: line-through;
        }

        :host( :not( [is-member] ) ) #mem-price {
            text-decoration: line-through;
        }

        .price {
            white-space: pre;
        }

        .price[hide='true'] {
            display: none;
        }

        .price > span {
            font-weight: 600;
        }

    </style>
    
    <div id="item-registration">
        <div id='title-container'>
            <input id='title-checkbox' type='checkbox' part='mode-checkbox'>
            <label id='title' part='mode-item-title'></label>
        </div>
        <div id='prices'>
            <label id='reg-price' class='price' part='mode-price'>Regular Price: $<span></span></label>
            <label id='mem-price' class='price' part='mode-price'>Member Price: $<span></span></label>
            <label id='free-price' class='price' part='mode-price'>FREE</label>
        </div>
        <div id='qty-container'>
            <label id='qty' part='mode-qty-title'>qty: </label>
            <input id='qty-input' type='number' value=1 part='mode-qty-input'>
        </div>
    </div>         
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "odp-event-item-registration" );

export class OdpEventItemRegistration extends ODEventRegistrationMode {
    constructor() {
        super( template );
    }

    _init() {
        this._titleCheckbox = this.shadowRoot.querySelector( '#title-checkbox' );
        this._titleLabel = this.shadowRoot.querySelector( '#title' );

        this._regularPriceLabel = this.shadowRoot.querySelector( '#reg-price' );
        this._regularPriceSpan = this._regularPriceLabel.querySelector( 'span' );
        this._memberPriceLabel = this.shadowRoot.querySelector( '#mem-price' );
        this._memberPriceSpan = this._memberPriceLabel.querySelector( 'span' );
        this._freeLabel = this.shadowRoot.querySelector( '#free-price' );
        this._qtyInput = this.shadowRoot.querySelector( '#qty-input' );
    }

    connectedCallback() {
        super.connectedCallback();

        //listeners and others etc.
        this._titleCheckbox.addEventListener( 'change', ( e ) => {
            if( e.target.checked && this._quantity === 0 ) {
                this._updateQuantity( 1 );
            }
            this._dispatchChange();
        } );

        this._qtyInput.addEventListener( 'change', ( e ) => {
            this._updateQuantity( e.target.value );
        } );

        this._requestInlineHook();

        this.setAttribute( 'exportparts', 'mode-qty-input, mode-qty-title, mode-item-title, mode-price, mode-checkbox' );

        this._dispatchChange();
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return [
            'event-id',
            'registration-id',
            'regular-price',
            'member-price',
            'is-member',
            'show-checkbox',
            'checked',
            'title',
            'inline'
        ];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        super.attributeChangedCallback( attrName, oldValue, newValue );
        this._dispatchChange();
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////

    get invalidMessage() {
        return this.title + " must have a quantity greater than 0."
    }

    _setRegularPriceAttribute( price ) {        
        this._setNumberAttribute( 'regular-price', price );
        this._setRegularPriceInDom( this.regularPrice );
    }

    _setMemberPriceAttribute( price ) {        
        this._setNumberAttribute( 'member-price', price );
        this._setMemberPriceInDom( this.memberPrice );
    }

    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////

    _setRegularPriceInDom( price ) {
        if( isNaN( price ) || price === null ) {
            this._regularPriceLabel.setAttribute( 'hide', true );
        } else if( price === 0 ) {
            this._regularPriceLabel.removeAttribute( 'hide' );
            this._regularPriceSpan.innerHTML = 'FREE';
        } else {
            this._regularPriceLabel.removeAttribute( 'hide' );
            this._regularPriceSpan.innerHTML = price.toFixed(2);
        }
        this._checkFree();
    }

    _setMemberPriceInDom( price ) {
        if( isNaN( price ) || price === null ) {
            this._memberPriceLabel.setAttribute( 'hide', true );
        } else if( price === 0 ) {
            this._memberPriceLabel.removeAttribute( 'hide' );
            this._memberPriceSpan.innerHTML = 'FREE';
        } else {
            this._memberPriceLabel.removeAttribute( 'hide' );
            this._memberPriceSpan.innerHTML = price.toFixed(2);
        }
        this._checkFree();
    }

    _updateQuantity( qty ) {
        this._qtyInput.value = qty;
        this._quantity = Number.parseInt( qty );
        this.checked = this._quantity > 0;
        this._dispatchChange();
    }

    _checkFree() {
        if( this.memberPrice === 0 && this.regularPrice === 0 ) {
            this._regularPriceLabel.setAttribute( 'hide', true );            
            this._memberPriceLabel.setAttribute( 'hide', true );
        }
        this._freeLabel.setAttribute( 'hide', !this._regularPriceLabel.hasAttribute( 'hide' ) || !this._memberPriceLabel.hasAttribute( 'hide' ) );
    }    

    _calculateTotals() {
        let cost = 0.00;
        let discount = 0.00;
        if( this.isMember ) {
            cost = this.memberPrice;
            discount = this.regularPrice - this.memberPrice;
        } else {
            cost = this.regularPrice;
        }
        if( isNaN( cost ) ) {
            cost = 0;
        }
        if( isNaN( discount ) ) {
            discount = 0;
        }
        this._total = Number.parseInt( this._quantity ) * Number.parseFloat( cost );
        this._discount = Number.parseInt( this._quantity ) * Number.parseFloat( discount );
    }

    _dispatchChange() {
        this.shadowRoot.dispatchEvent( new CustomEvent(
            'item-registration-change',
            {
                bubbles: true,
                composed: true,
                'detail': this.getData()
            } 
        ) );
    }

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////

    validate() {
        return Number.parseFloat( this._qtyInput.value ) > 0;
    }

    getData() {
        this._calculateTotals();
        return {
            'eventId': this.eventId,
            'registrationId': this.registrationId,
            'title': this.title,
            'quantity': this._quantity,
            'discount': this.totalDiscount,
            'total': this.totalCost,
            'checked': this.checked,
            'isMember': this.isMember,
            'type': 4
        };
    }
}
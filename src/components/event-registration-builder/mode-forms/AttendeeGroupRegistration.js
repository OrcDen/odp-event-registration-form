import ODEventRegistrationMode from './lib/eventRegistrationMode.js';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        :host {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        #attendee-group-registration {
            display: flex;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        :host( :not( [show-line] ) ) hr {
            display: none;
        }

        :host( :not( [show-delete] ) ) .delete-button {
            display: none;
        }
        
        :host( :not( [show-title] ) ) #title-container {
            display: none;
        }

        #title-container {
            display: flex;
            align-items: flex-end;
        }

        hr {
            display: flex;
            width: 100%;
        }
    
        #title {
            margin: 0 1rem;
            font-weight: 600;
            font-size: 1.2rem;
        }

        #group-title {
            font-weight: 600;
            font-size: 1.1rem;
        }

        #attendee-container {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 1rem;
            padding-right: 0;
        }

        .delete-button::after {
            display: block;
            content: 'X';
        }

        .container {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

    </style>
    
    <hr part='divider'>
    <div id="attendee-group-registration">
        <div class='container'>            
            <div id='title-container'>
                <label id='group-title' part='mode-attendee-group-title'>Attendee Group <span></span>:</label>
            </div>
            <div id='attendee-container'>
                <slot id='attendees'></slot>
            </div>            
        </div>
        <button class='delete-button' part='mode-delete-button'></button>
    </div>         
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "odp-event-attendee-group-registration" );

export class OdpEventAttendeeGroupRegistration extends ODEventRegistrationMode {
    constructor() {
        super( template );
    }

    _init() {
        this._storedData = null;

        this._attendeeGroupInfo = { 'attendees': [] };
        this._childrenAttendees = [];

        this._hasSameAsContact = false;

        this._addButton = this.shadowRoot.querySelector( '#add-button' );
        this._deleteButton = this.shadowRoot.querySelector( '.delete-button' );

        this._slot = this.shadowRoot.querySelector( '#attendees' );
    }

    connectedCallback() {
        super.connectedCallback();
        //set attribute default values first and pass to element
        this.setAttribute( 'group-price', true );
        this.setAttribute( 'checked', true );
        this.setAttribute( 'show-delete', true );
        this.setAttribute( 'show-line', true );
        this.setAttribute( 'show-title', true );
        if( !this.hasAttribute( 'index' ) ) {
            this.setAttribute( 'index', 0 )
        }

        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( 'index' );
        this._upgradeProperty( 'showContactCheckbox' );
        this._upgradeProperty( 'showDelete' );
        this._upgradeProperty( 'showLine' );
        this._upgradeProperty( 'showTitle' );
        this._upgradeProperty( 'groupPrice' );

        //listeners and others etc.
        this.shadowRoot.addEventListener( 'attendee-registration-change', ( e ) => this._updateAttendees( e ) );
        this.shadowRoot.addEventListener( 'attendee-is-same-change', ( e ) => this._updateHasSame( e ) );

        let config = { childList: true, subtree: true };
        this._observer = new MutationObserver( ( mutationsList, observer ) => { 
            for( let i = 0; i < mutationsList.length; i++ ) {
                let record = mutationsList[i];
                if( record.addedNodes && record.addedNodes.length ) {
                    for( let j = 0; j < record.addedNodes.length; j++ ) {
                        let node = record.addedNodes[j];
                        if( node.tagName === 'ODP-EVENT-ATTENDEE-REGISTRATION' ) {
                            this._childrenAttendees.push( node );
                        }
                    }
                }
                if( record.removedNodes && record.removedNodes.length ) {
                    for( let j = 0; j < record.removedNodes.length; j++ ) {
                        let node = record.removedNodes[j];
                        if( node.tagName === 'ODP-EVENT-ATTENDEE-REGISTRATION' ) {
                            this._childrenAttendees.splice( node.index, 1 );
                            this._attendeeGroupInfo.attendees.splice( node.index, 1);
                        }
                    }
                }
            }
                      
            this.updateAttendeeIndices();
            this._mutationObserverCallback( mutationsList, observer );
            this._dispatchChange();
        } );
        this._observer.observe( this , config );

        this.setAttribute( 'exportparts', 'mode-attendee-group-title, mode-attendee-title, divider, mode-checkbox, mode-delete-button, mode-attendee-form, mode-attendee-table, mode-attendee-table-row, mode-attendee-table-cell, mode-attendee-label-cell, mode-attendee-field-label, mode-attendee-field-label-row, mode-attendee-value-label, mode-attendee-form-input, mode-attendee-form-select, mode-attendee-form-textarea, mode-attendee-form-richtext, mode-attendee-richtext-editor' );

        this._dispatchChange();
        this._dispatchIsSame();
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return [
            'event-id',
            'registration-id',
            'regular-price',
            'member-price',
            'is-member',
            'show-checkbox',
            'checked',
            'index',
            'show-contact-checkbox',
            'show-delete',
            'show-line',
            'group-price'];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        super.attributeChangedCallback( attrName, oldValue, newValue );
        switch ( attrName ) {
            case "index": {
                if ( newValue !== null && newValue !== "" && !this._validateInt( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setIndexAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setIndexAttribute( newValue );
                }
                break;
            }
            case "show-contact-checkbox": {
                if ( newValue !== null && newValue !== "" && !this._validateInt( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setShowContactCheckboxAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setShowContactCheckboxAttribute( newValue );
                }
                break;
            }
            case "show-delete": {
                if ( newValue !== null && newValue !== "" && !this._validateInt( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setShowDeleteAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setShowDeleteAttribute( newValue );
                }
                break;
            }
            case "show-line": {
                if ( newValue !== null && newValue !== "" && !this._validateInt( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setShowLineAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setShowLineAttribute( newValue );
                }
                break;
            }
            case "show-title": {
                if ( newValue !== null && newValue !== "" && !this._validateInt( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setShowTitleAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setShowTitleAttribute( newValue );
                }
                break;
            }
            case "group-price": {
                if ( newValue !== null && newValue !== "" && !this._validateInt( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setGroupPriceAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setGroupPriceAttribute( newValue );
                }
                break;
            }
        }
        this._dispatchChange();
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////

    get index() {
        return Number.parseInt( this.getAttribute( "index" ) );
    }

    set index( index ) {
        if ( typeof index !== "number" ) {
            return;
        }
        this._setIndexAttribute( index );
    }

    _setIndexAttribute( index ) {        
        this._setNumberAttribute( 'index', index );
        this.shadowRoot.querySelector( '#group-title > span' ).innerHTML = this.index + 1;
    }

    get showContactCheckbox() {
        return this.hasAttribute( 'show-contact-checkbox' );
    }

    set showContactCheckbox( show ) {
        if( typeof show !== 'boolean' ) {
            return;
        }
        this._setShowContactCheckboxAttribute( show );
    }

    _setShowContactCheckboxAttribute( show ) {
        this._setBooleanAttribute( 'show-contact-checkbox', show );
        if( this._childrenAttendees.length > 0 ) {
            this._childrenAttendees[0].setAttribute( 'show-contact-checkbox', show );
        }
    }

    get showDelete() {
        return this.hasAttribute( 'show-delete' );
    }

    set showDelete( show ) {
        if( typeof show !== 'boolean' ) {
            return;
        }
        this._setShowDeleteAttribute( show );
    }

    _setShowDeleteAttribute( show ) {
        this._setBooleanAttribute( 'show-delete', show );
    }

    get showLine() {
        return this.hasAttribute( 'show-line' );
    }

    set showLine( show ) {
        if( typeof show !== 'boolean' ) {
            return;
        }
        this._setShowLineAttribute( show );
    }

    _setShowLineAttribute( show ) {
        this._setBooleanAttribute( 'show-line', show );
    }

    get showTitle() {
        return this.hasAttribute( 'show-title' );
    }

    set showTitle( show ) {
        if( typeof show !== 'boolean' ) {
            return;
        }
        this._setShowTitleAttribute( show );
    }

    _setShowTitleAttribute( show ) {
        this._setBooleanAttribute( 'show-title', show );
    }  

    get groupPrice() {
        return this.hasAttribute( 'group-price' );
    }

    set groupPrice( useGroupPrice ) {
        if( typeof useGroupPrice !== 'boolean' ) {
            return;
        }
        this._setGroupPriceAttribute( useGroupPrice );
    }

    _setGroupPriceAttribute( useGroupPrice ) {
        this._setBooleanAttribute( 'group-price', useGroupPrice );
    }

    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    
    _mutationObserverCallback( mutationsList, observer ) {
        this.dispatchEvent(
            new CustomEvent( 'odp-event-attendee-group-mutation', { 
                bubbles: true, 
                detail: { 
                    'mutationsList': mutationsList,
                    'observer': observer
                }
            } )
        );
    }

    mutationPromiseCallback( e, resolve ) {
        e.stopPropagation();
        this.removeEventListener( 'odp-event-attendee-group-mutation', ( e ) => { this.mutationPromiseCallback( e, resolve ); } );
        resolve( e.detail ); 
    }    

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////

    _updateAttendees( e ) {
        e.stopPropagation();
        if( isNaN( e.detail.index ) ) {
            return;
        }
        while( !this._attendeeGroupInfo.attendees[e.detail.index] ) {
            this._attendeeGroupInfo.attendees.push( {} );
        }
        this._attendeeGroupInfo.attendees[e.detail.index] = this._copy.deepCopy( e.detail.attendeeInfo );
        this._dispatchChange();
    }

    _setFirstAttendee( attendeeObject ) {
        if( this._childrenAttendees[0] ) {
            this._childrenAttendees[0].setData( attendeeObject );
        }
    }

    _updateHasSame( e ) {
        e.stopPropagation();
        this._hasSameAsContact = e.detail.isSameAsContact;
        this._dispatchIsSame();
    }

    _calculateTotals() {
        let cost = 0.00;
        let discount = 0.00;
        this._calculateQuantity();
        if( this.isMember ) {
            cost = this.memberPrice;
            discount = this.regularPrice - this.memberPrice;
        } else {
            cost = this.regularPrice;
        }
        this._total = Number.parseInt( this._quantity ) * Number.parseFloat( cost );
        this._discount = Number.parseInt( this._quantity ) * Number.parseFloat( discount );
    }

    _calculateQuantity() {
        if( this.groupPrice ) {
            this._quantity = 1;
        } else {
            this._quantity = this._childrenAttendees.length; 
        }
    }

    _dispatchIsSame() {
        this.shadowRoot.dispatchEvent( new CustomEvent(
            'group-has-same-change',
            {
                bubbles: true,
                composed: true,
                'detail': {
                    'hasSameAsContact': this._hasSameAsContact
                }
            } 
        ) );
    }

    _dispatchChange() {
        let newData = this._copy.deepCopy( this.getData() );
        if( this._same.isSame( this._storedData, newData ) ) {
            return;
        }
        this._storedData = newData;
        this.shadowRoot.dispatchEvent( new CustomEvent(
            'attendee-group-registration-change',
            {
                bubbles: true,
                composed: true,
                'detail': this._storedData
            } 
        ) );
    }

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////

    validate() {
        return true;
    }


    configureDeleteButton( callback, parent ) {
        this._deleteButton.onclick = async () => {
            await callback( parent, this );
        };
        this.showDelete = true;
    }

    getData() {
        this._calculateTotals();
        return {
            'attendeeGroupInfo': this._attendeeGroupInfo,
            'eventId': this.eventId,
            'registrationId': this.registrationId,
            'index': this.index,
            'discount': this.totalDiscount,
            'total': this.totalCost,
            'checked': this.checked
        }
    }

    async mutationPromise() {
        return new Promise( ( resolve ) => { 
            this.addEventListener( 'odp-event-attendee-group-mutation', ( e ) => { this.mutationPromiseCallback( e, resolve ); } );
        } );
    }

    updateAttendeeIndices() {
        for( let i = 0; i < this._childrenAttendees.length; i++ ) {
            this._childrenAttendees[i].index = i;
        }
    }

    //try remove
    gengerateIndex() {
        let parent = this.parentElement;
        let tempCount = 0;
        for( let j = 0; j < parent.children.length; j++) {
            let temp = parent.children[j];
            if( !temp || temp === this ) {
                continue;
            }
            if( temp.tagName === 'ODP-EVENT-ATTENDEE-GROUP-REGISTRATION' && temp.registrationId === this.registrationId && temp.eventId === this.eventId ) {
                tempCount += 1;
            }
        }
        this.index = tempCount;
    }
}
import ODEventRegistrationMode from './lib/eventRegistrationMode.js';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        :host {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        #variable-registration {
            display: flex;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            align-items: center;
        }
    
        :host( :not( [show-checkbox] ) ) #title-checkbox {
            display: none;
        }   
        
        :host( :not( [inline] ) ) #variable-registration {
            flex-direction: column;
            align-items: flex-start;
        }

        #title-container {
            display: flex;
            align-items: center;
        }
    
        #title {
            margin: 0 1rem;
            font-weight: 600;
            font-size: 1.1rem;
        }

        #amount {
            white-space: pre;
            margin-left: 2rem;
        }

        #amount-input {
            width: 5rem;
        }

        #qty-container {
            display: flex;
        }

        #prices {
            display: flex;
            flex-direction: column;

            margin-left: 2rem;
        }

        :host( [is-member] ) #reg-price {
            text-decoration: line-through;
        }

        :host( :not( [is-member] ) ) #mem-price {
            text-decoration: line-through;
        }

        .price {
            white-space: pre;
        }

        .price[hide='true'] {
            display: none;
        }

        .price > span {
            font-weight: 600;
        }

    </style>
    
    <div id="variable-registration">
        <div id='title-container'>
            <input id='title-checkbox' type='checkbox' part='mode-checkbox'>
            <label id='title' part='mode-item-title'></label>
        </div>
        <div id='qty-container'>
            <label id='amount' part='mode-qty-title'>Amount: $</label>
            <input id='amount-input' type='number' value='0.00' part='mode-qty-input' min='0.01' step='0.01'>
        </div>
    </div>         
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "odp-event-variable-registration" );

export class OdpEventVarliableRegistration extends ODEventRegistrationMode {
    constructor() {
        super( template );
    }

    _init() {
        this._titleCheckbox = this.shadowRoot.querySelector( '#title-checkbox' );
        this._titleLabel = this.shadowRoot.querySelector( '#title' );

        this._amountInput = this.shadowRoot.querySelector( '#amount-input' );
    }

    connectedCallback() {
        super.connectedCallback();

        //listeners and others etc.
        this._amountInput.addEventListener( 'change', ( e ) => {
            this._total = Math.round( Number.parseFloat( e.target.value ) * 100 ) / 100;
            this.checked = this._total > 0;
            this._dispatchChange();
        } );

        this._requestInlineHook();

        this.setAttribute( 'exportparts', 'mode-qty-input, mode-qty-title, mode-item-title, mode-checkbox' );

        this._dispatchChange();
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return [
            'event-id',
            'registration-id',
            'is-member',
            'show-checkbox',
            'checked',
            'title',
            'inline'
        ];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        super.attributeChangedCallback( attrName, oldValue, newValue );
        this._dispatchChange();
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////

    get invalidMessage() {
        return this.title + " must have an amount greater than 0."
    }

    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////

    _dispatchChange() {
        this.shadowRoot.dispatchEvent( new CustomEvent(
            'variable-registration-change',
            {
                bubbles: true,
                composed: true,
                'detail': this.getData()
            } 
        ) );
    }

    _calculateTotals() {
        return; //override and do nothing
    }

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////

    validate() {
        return Number.parseFloat( this._amountInput.value ) > 0;
    }

    getData() {
        return {
            'eventId': this.eventId,
            'registrationId': this.registrationId,
            'title': this.title,
            'total': this.totalCost,
            'checked': this.checked,
            'quantity': this.totalCost,
            'type': 5
        }
    }

}
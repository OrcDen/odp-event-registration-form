import ODEventParentRegistrationMode from './lib/eventParentRegistrationMode.js';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        :host {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        #parent-single-registration {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        :host( :not( [show-checkbox] ) ) #title-checkbox {
            display: none;
        }

        :host( :not( [inline] ) ) #item-container {
            padding-left: 1rem;
        }

        .container {
            display: flex;
            align-items: center;
        }

        #title-container {
            display: flex;
            align-items: center;
        }
    
        #title {
            font-weight: 600;
            font-size: 1.25rem;
            margin: 0 1rem;
        }

        #item-container {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding-left: 4rem;
            margin-bottom: 1rem;
        }

    </style>
    
    <div id="parent-single-registration">
        <div id='title-container'>
            <input id='title-checkbox' type='checkbox' part='mode-checkbox'>
            <label id='title' part='mode-item-group-title'></label>
        </div>
        <div id='item-container'>
            <slot id='items'></slot>
        </div>
    </div>         
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "odp-event-parent-single-registration" );

export class OdpEventParentSingleRegistration extends ODEventParentRegistrationMode {
    constructor() {
        super( template );
    }

    _init() {
        this._children = [];

        this._titleLabel = this.shadowRoot.querySelector( '#title' );
        this._titleCheckbox = this.shadowRoot.querySelector( '#title-checkbox' );

        this._itemsSlot = this.shadowRoot.querySelector( '#items' );

        this._invalidMessage = '';
    }

    connectedCallback() {
        super.connectedCallback();        

        //set attribute default values first and pass to element

        //all properties should be upgraded to allow lazy functionality

        //listeners and others etc.
        let config = { attributes: false, childList: true, subtree: true };
        this._observer = new MutationObserver( ( mutationsList, observer ) => {        
            for( let i = 0; i < mutationsList.length; i++ ) {
                let record = mutationsList[i];
                if( record.addedNodes && record.addedNodes.length ) {
                    for( let j = 0; j < record.addedNodes.length; j++ ) {
                        let node = record.addedNodes[j];
                        if( node.parentElement !== this ) {
                            continue;
                        }
                        if( node.classList.contains( 'radio-container') ) {
                            this._children.push( node );
                        }
                    }
                }
                if( record.removedNodes && record.removedNodes.length ) {
                    for( let j = 0; j < record.removedNodes.length; j++ ) {
                        let node = record.removedNodes[j];
                        if( node.classList.contains( 'radio-container') ) {                          
                            for( let k = 0; k < this._children.length; k++ ) {
                                if( node === this._children[k] ) {
                                    this._children.splice( k, 1 );
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            this._mutationObserverCallback( mutationsList, observer );  
            this._dispatchChange();  
        } );
        this._observer.observe( this , config );

        this._requestInlineHook();

        this.setAttribute( 'exportparts', 'mode-radio, mode-item-group-title, mode-add-button, mode-qty-input, mode-qty-title, mode-item-title, mode-price, mode-attendee-group-title, mode-attendee-title, divider, mode-checkbox, mode-delete-button, mode-attendee-form, mode-attendee-table, mode-attendee-table-row, mode-attendee-table-cell, mode-attendee-label-cell, mode-attendee-input-cell, mode-attendee-field-label, mode-attendee-field-label-row, mode-attendee-value-label, mode-attendee-form-input, mode-attendee-form-select, mode-attendee-form-textarea, mode-attendee-form-richtext, mode-attendee-richtext-editor, mode-multi-form, mode-multi-table, mode-multi-table-row, mode-multi-table-cell, mode-multi-label-cell, mode-multi-input-cell, mode-multi-field-label, mode-multi-field-label-row, mode-multi-value-label, mode-multi-form-input, mode-multi-form-select, mode-multi-form-textarea, mode-multi-form-richtext, mode-multi-richtext-editor' );

        this._dispatchChange();
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return [
            'event-id',
            'registration-id',
            'is-member',
            'show-checkbox',
            'checked',
            'title',
            'inline'
        ];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        super.attributeChangedCallback( attrName, oldValue, newValue );
        this._dispatchChange();
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////

    get invalidMessage() {
        return this._invalidMessage;
    }

    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    
    _mutationObserverCallback( mutationsList, observer ) {
        this.dispatchEvent(
            new CustomEvent( 'odp-event-single-mutation', { 
                bubbles: true, 
                detail: { 
                    'mutationsList': mutationsList,
                    'observer': observer
                }
            } )
        );
    }

    mutationPromiseCallback( e, resolve ) {
        e.stopPropagation();
        this.removeEventListener( 'odp-event-single-mutation', ( e ) => { this.mutationPromiseCallback( e, resolve ); } );
        resolve( e.detail ); 
    }    

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////


    _updateSingleVariableOrItem( e ) {
        let childItemRegistration = {
            'eventId': e.detail.eventId,
            'registrationId': e.detail.registrationId,
            'quantity': e.detail.quantity,
            'attendeeGroups': null,
            'childRegistrations': null
        }
        this._updateRegistrationInfo( childItemRegistration, e.detail.checked );
    }

    _updateItemGroupRegistration( e ) {
        let newRegistration = this._copy.deepCopy( e.detail.registrationInfo );
        this._updateRadioChecked( e, newRegistration );
        this._updateRegistrationInfo( newRegistration );
    }

    _updateMultiRegistration( e ) {
        let newRegistration = this._copy.deepCopy( e.detail.registrationInfo );
        this._updateRadioChecked( e, newRegistration );
        this._updateRegistrationInfo( newRegistration );
    }

    _updateParentSingleRegistration( e ) {
        let newRegistration = this._copy.deepCopy( e.detail.registrationInfo );
        this._updateRadioChecked( e, newRegistration );
        this._updateRegistrationInfo( newRegistration );
    }

    _updateRegistrationInfo( registration ) {
        if( this._registrationInfo.childRegistrations.length === 0 ) {
            this._registrationInfo.childRegistrations.push( {} );
        }
        this._registrationInfo.childRegistrations.splice( 0, 1 );
        this._registrationInfo.childRegistrations.push( registration );
        this._dispatchChange();
    }

    _updateRadioChecked( e, newRegistration ) {
        if( Object.keys( newRegistration.childRegistrations ).length > 0 ) {
            e.target.parentElement.firstChild.checked = true;
            this.checked = true;
        } else {
            for( let i = 0; i < newRegistration.attendeeGroups.length; i++ ) {
                let group = newRegistration.attendeeGroups[i];
                for( let j = 0; j < group.attendees.length; j++ ) {
                    let attendee = group.attendees[j];
                    if( Object.keys( attendee ).length > 0 ) {                        
                        e.target.parentElement.firstChild.checked = true;
                        this.checked = true;
                    }
                    
                }
            }
        }
    }

    _calculateTotals() {
        let cost = 0.00;
        let discount = 0.00;
        for( let i = 0; i < this._children.length; i++ ) {
            let radio = this._children[i].firstChild;
            let child = this._children[i].firstChild.nextSibling;
            if( !radio || !radio.checked || radio.checked === undefined ) {
                continue;
            }
            discount += child.totalDiscount;              
            cost += child.totalCost;
        }
        this._total = Number.parseInt( this._quantity ) * Number.parseFloat( cost );
        this._discount = Number.parseInt( this._quantity ) * Number.parseFloat( discount );
    }

    _dispatchChange() {
        this.__dispatchRegInfoChange( 'parent-single-registration-change' );
    }

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////

    validate() {
        let valid = true;
        let oneChecked = false;
        let messgageSet = false;
        for( let i = 0; i < this._children.length; i++ ) {
            let child = this._children[i];
            let checkbox = child.firstChild;
            let item = child.firstChild.nextSibling;
            if( checkbox.checked ) {
                oneChecked = true;
                if( !item.validate() ) {
                    if( !messgageSet ) {
                        this._invalidMessage = item.invalidMessage;
                        messgageSet = true;
                    }
                    valid = false;
                }
            }            
        }
        if( !oneChecked ) {
            valid = false;
            this._invalidMessage = this.title + ' must have at least one option selected.'
        }
        return valid;
    }

    async mutationPromise() {
        return new Promise( ( resolve ) => { 
            this.addEventListener( 'odp-event-single-mutation', ( e ) => { this.mutationPromiseCallback( e, resolve ); } );
        } );
    }
}
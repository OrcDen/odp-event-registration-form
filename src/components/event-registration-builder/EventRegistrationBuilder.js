import './mode-forms';
import ModeBuilder from './mode-forms/lib/ModeBuilder.js';
import Copy from '../../lib/copy.js';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        :host {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        #mode-container {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .attendee-group-container {
            display: flex;
            align-items: flex-start;
        }

        .attendee-group-container:first-child > button {
            display: none;
        }

        .attendee-group-container > button {
            margin: 2rem;
        }

        .attendee-group-container > button::after {
            display: block;
            content: 'X';
        }

        .attendee-container {
            display: flex;
            align-items: flex-start;
        }

        .attendee-container > button {
            margin: 2rem;
        }

        .attendee-container > button::after {
            display: block;
            content: 'X';
        }

    </style>
    
    <div id='mode-container'></div>       
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "odp-event-registration-builder" );

export class OdpEventRegistrationBuilder extends HTMLElement {
    constructor() {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {

        this._invalidMessage = '';

        this._modeBuilder = new ModeBuilder( this );
        this._copy = new Copy();

        this._registrationInfo = {};

        this._total = 0.00;
        this._discount = 0.00;
        this._quantity = 1;

        this._regItems = [];
        this._eventData = null;
        this._eventId = 0;

        this._modeContainer = this.shadowRoot.querySelector( '#mode-container' );
        this._button = this.shadowRoot.querySelector( '#button-container > button' );

        //set attribute default values first and pass to element

        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( 'eventData' );

        // mutations
        let config = { attributes: false, childList: true, subtree: true };
        this._observer = new MutationObserver( ( mutationsList, observer ) => {        
            for( let i = 0; i < mutationsList.length; i++ ) {
                let record = mutationsList[i];
                if( record.addedNodes && record.addedNodes.length ) {
                    for( let j = 0; j < record.addedNodes.length; j++ ) {
                        let node = record.addedNodes[j];
                    }
                }
                if( record.removedNodes && record.removedNodes.length ) {
                    for( let j = 0; j < record.removedNodes.length; j++ ) {
                        let node = record.removedNodes[j];
                    }
                }
            }
            this._mutationObserverCallback( mutationsList, observer );   
        } );
        this._observer.observe( this._modeContainer , config );

        //listeners and others etc.
        this.addEventListener( 'parent-item-group-registration-change', ( e ) => {
            e.stopPropagation();
            this._updateParentMultiOrItemGroupRegistration( e );
        } );
        
        this.addEventListener( 'parent-single-registration-change', ( e ) => {
            e.stopPropagation();
            this._updateParentMultiOrItemGroupRegistration( e );
        } );

        this.addEventListener( 'parent-multi-registration-change', ( e ) => {
            e.stopPropagation();
            this._updateParentMultiOrItemGroupRegistration( e );
        } );

        this.addEventListener( 'item-registration-change', ( e ) => {
            e.stopPropagation();
            this._updateSingleVariableOrItem( e );
        } );

        this.addEventListener( 'variable-registration-change', ( e ) => {
            e.stopPropagation();
            this._updateSingleVariableOrItem( e );
        } );

        this.setAttribute( 'exportparts', 'mode-radio, mode-item-group-title, mode-add-button, mode-qty-input, mode-qty-title, mode-item-title, mode-price, mode-attendee-group-title, mode-attendee-title, divider, mode-checkbox, mode-delete-button, mode-attendee-form, mode-attendee-table, mode-attendee-table-row, mode-attendee-table-cell, mode-attendee-label-cell, mode-attendee-input-cell, mode-attendee-field-label, mode-attendee-field-label-row, mode-attendee-value-label, mode-attendee-form-input, mode-attendee-form-select, mode-attendee-form-textarea, mode-attendee-form-richtext, mode-attendee-richtext-editor, mode-multi-form, mode-multi-table, mode-multi-table-row, mode-multi-table-cell, mode-multi-label-cell, mode-multi-input-cell, mode-multi-field-label, mode-multi-field-label-row, mode-multi-value-label, mode-multi-form-input, mode-multi-form-select, mode-multi-form-textarea, mode-multi-form-richtext, mode-multi-richtext-editor' );

        this._build();
    }

    //General - every element should have this
    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ['is-member'];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////

    get eventData() {
        return this._eventData;
    }

    set eventData( data ) {
        this._eventData = data;
        if( !this._eventData ) {            
            return;
        }
        this._eventId = this._eventData.id;
        this._reset();
        this._build();
    }

    get eventId() {
        return this._eventId
    }

    get totalCost() {
        this._calculateTotals();
        return this._total;
    }

    get totalDiscount() {
        this._calculateTotals();
        return this._discount;
    }

    get invalidMessage() {
        return this._invalidMessage;
    }

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    } 

    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    
    _mutationObserverCallback( mutationsList, observer ) {
        this.dispatchEvent(
            new CustomEvent( 'odp-event-registration-builder-mutation', { 
                bubbles: true, 
                detail: { 
                    'mutationsList': mutationsList,
                    'observer': observer
                }
            } )
        );
    }

    mutationPromiseCallback( e, resolve ) {
        e.stopPropagation();
        this.removeEventListener( 'odp-event-registration-builder-mutation', ( e ) => { this.mutationPromiseCallback( e, resolve ); } );
        resolve( e.detail ); 
    }

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////
    
    async _build() {
        if( !this.eventData || !this.eventData.registrationData ) {
            return;
        }
        let regItem = this.eventData.registrationData[0];
        if( !regItem || regItem.registrationType === 0 ) {
            return;
        }
        let element = await this._modeBuilder.buildRegItem( regItem, this.eventData, this._modeContainer, async () => await this.mutationPromise() );
        this._modeBuilder.configureFirstItem( element, regItem );

        this._regItems.push( element );
        this._dispatchChange();
    }

    _reset() {
        while( this._modeContainer.firstChild ) {
            this._modeContainer.removeChild( this._modeContainer.firstChild )
        }
        while( this._regItems.length > 0 ) {
            this._regItems.splice( 0, 1 );
        }
        this._registrationInfo = {};
    }
    
    _updateParentMultiOrItemGroupRegistration( e ) {
        this._registrationInfo['eventId'] = e.detail.eventId;
        this._registrationInfo['registrationId'] = e.detail.registrationId;

        this._registrationInfo['attendeeGroups'] = this._copy.copyArray( e.detail.registrationInfo.attendeeGroups );

        this._registrationInfo['childRegistrations'] = this._copy.copyArray( e.detail.registrationInfo.childRegistrations );
        this._quantity = this._registrationInfo['childRegistrations'].length + this._registrationInfo['attendeeGroups'].length;
        this._registrationInfo['quantity'] = this._quantity;

        this._dispatchChange();
    }

    _updateSingleVariableOrItem( e ) {
        if( e.detail.checked ) {
            this._updateCommon( e );
            this._registrationInfo['attendeeGroups'] = null;
            this._registrationInfo['childRegistrations'] = null;
            this._total = e.detail.total;
        } else {
            this._registrationInfo = {};
            this._total = 0.00;
        }
        this._dispatchChange();
    }

    _updateCommon( e ) {
        this._registrationInfo['eventId'] = this.eventId;
        this._registrationInfo['registrationId'] = e.detail.registrationId;
        this._registrationInfo['quantity'] = e.detail.quantity;
    }

    _calculateTotals() {
        let totalTemp = 0.00;
        let discountTemp = 0.00;
        for( let i = 0; i < this._regItems.length; i++ ) {
            let temp = this._regItems[i];
            if( temp.checked === false ) {
                continue;
            }
            if( temp.totalCost !== undefined ) {
                totalTemp += temp.totalCost;
            }
            if( temp.totalDiscount !== undefined ) {
                discountTemp += temp.totalDiscount;
            }            
        }
        this._total = totalTemp;
        this._discount = discountTemp;
    }
    
    _dispatchChange() {
        this.shadowRoot.dispatchEvent( new CustomEvent(
            'registration-builder-change',
            {
                bubbles: true,
                composed: true,
                'detail': this.getData()
            } 
        ) );
    }

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////

    validate() {
        let valid = true;
        for( let i = 0; i < this._regItems.length; i ++ ) {
            let item = this._regItems[i];
            if( !item.validate() ) {
                valid = false;
                this._invalidMessage = item.invalidMessage;
            }
        }
        return valid;
    }

    async mutationPromise() {
        return new Promise( ( resolve ) => { 
            this.addEventListener( 'odp-event-registration-builder-mutation', ( e ) => { this.mutationPromiseCallback( e, resolve ); } );
        } );
    }

    getData() {
        this._calculateTotals();
        return {
            'registrationInfo': this._registrationInfo,
            'total': this._total,
            'discount': this._discount,
            'quantity': this._quantity
        }
    }
}
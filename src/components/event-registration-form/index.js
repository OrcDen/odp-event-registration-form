import { OdpEventRegistrationForm } from "./EventRegistrationForm.js";
if ( !customElements.get( "odp-event-registration-form" ) ) {
    window.customElements.define( "odp-event-registration-form", OdpEventRegistrationForm );
}
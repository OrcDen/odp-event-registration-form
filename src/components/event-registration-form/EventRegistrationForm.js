import '../contact-info-form';
import '../event-registration-builder';
import '../review-order-form';
import '../../lib/copy.js' 
import Copy from '../../lib/copy.js';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        :host {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        :host( [closed] ) #event-registration-form-inner {
            display: none;
        }

        :host( :not( [closed] ) ) #closed {
            display: none;
        }

        #event-registration-form {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        #event-registration-form-inner {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        hr {
            dipsplay: flex;
            width: 100%;
        }

    </style>
    
    <div id="event-registration-form">
        <h1 part='event-title' id='event-title'></h1>
        <div id='event-registration-form-inner'>
            <h2 part='contact-title' >Contact Information</h2>
            <odp-contact-info-form id='contact-info-form'></odp-contact-info-form>
            
            <hr part='divider'>
            <odp-event-registration-builder id='registration-builder' ></odp-event-registration-builder>

            <hr part='divider'>
            <h2 part='review-title'>Review Order</h2>
            <odp-review-order-form id='review-order-form' ></odp-review-order-form>
        </div>
        <h2 id='closed'>Registration for this event is currently closed.</h2>     
    </div>
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "odp-event-registration-form" );

export class OdpEventRegistrationForm extends HTMLElement {
    constructor() {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {

        this._invalidMessage = '';

        this._copy = new Copy();
        
        this._container = this.shadowRoot.querySelector( '#event-registration-form' );

        this._title = this.shadowRoot.querySelector( '#event-title' );
        this._moneris = this.shadowRoot.querySelector( "#moneris-hosted-payment" );
        this._contact = this.shadowRoot.querySelector( "#contact-info-form" );
        this._review = this.shadowRoot.querySelector( "#review-order-form" );
        this._builder = this.shadowRoot.querySelector( "#registration-builder" );

        this._eventData = null;
        
        this._registrationInfo = {};
        this._eventRegistrationServiceObject = {
            'registrationInfo': this._registrationInfo
        };

        this._subtotal = 0.00;
        this._tax = 0.00;
        this._total = 0.00;

        this._memberHooks = [];
        this._inlineHooks = [];
        this._simpleFormHooks = [];
        this._registrationInfoHooks = [];

        this._storedContactInfoEvent;

        //set attribute default values first and pass to element

        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( 'simpleForm' );
        this._upgradeProperty( 'closed' );
        this._upgradeProperty( 'inline' );
        this._upgradeProperty( 'isMember' );
        this._upgradeProperty( 'eventData' );

        //listeners and others etc.
        this.shadowRoot.addEventListener( 'contact-info-change', ( e ) => { this._updateContactInfo( e ) } );
        this.shadowRoot.addEventListener( 'add-registration-info-hook', ( e ) => { this._addRegistrationInfoHook( e ) } );
        this.shadowRoot.addEventListener( 'add-member-hook', ( e ) => { this._addMemberHook( e ) } );
        this.shadowRoot.addEventListener( 'add-inline-hook', ( e ) => { this._addInlineHook( e ) } );
        this.shadowRoot.addEventListener( 'add-simple-form-hook', ( e ) => { this._addSimpleFormHook( e ) } );
        this.shadowRoot.addEventListener( 'registration-builder-change', ( e ) => {
            this._updateTotals( e.detail.total ); 
            this._updateRegistrationObject( e );
        } );

        if( this.eventData ) {
            this._builder.eventData = this.eventData;
        }
        
        this._contact.inline = this.inline;
    } 

    //General - every element should have this
    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ['inline','is-member','closed','simple-form'];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        switch ( attrName ) {
            case "inline": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setInlineAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setInlineAttribute( newValue );
                }
                break;
            }
            case "is-member": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setIsMemberAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setIsMemberAttribute( newValue );
                }
                break;
            }
            case "closed": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setClosedAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setClosedAttribute( newValue );
                }
                break;
            }
            case "simple-form": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setSimpleFormAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setSimpleFormAttribute( newValue );
                }
                break;
            }
        }
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////
    
    get eventData() {
        return this._eventData;
    }

    set eventData( data ) {
        this._eventData = data;
        if( !this.eventData ) {
            return;
        }        
        if( this.eventData.title ) {
            this._title.innerHTML = this.eventData.title
        }
        if( !this.eventData.registrationData || !this.eventData.registrationData[0] ) {
            this.closed = true;
            return;
        }
        if( this.eventData.registrationData[0].tax !== null ) {
            this._review.tax = this.eventData.registrationData[0].tax;
        }
        this._builder.eventData = this.eventData;
        
    }
    
    get invalidMessage() {
        return this._invalidMessage;
    }

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }

    get inline() {
        return this.hasAttribute( "inline" );
    }

    set inline( isInline ) {
        if ( typeof isInline !== "boolean" ) {
            return;
        }
        this._setInlineAttribute( isInline );
    }

    _setInlineAttribute( newV ) {
        this._setBooleanAttribute( 'inline', newV );
        this._contact.inline = this.inline;
        this._setInlineHooks();
    } 

    get isMember() {
        return this.hasAttribute( "is-member" );
    }

    set isMember( isMember ) {
        if ( typeof isMember !== "boolean" ) {
            return;
        }
        this._setIsMemberAttribute( isMember );
    }

    _setIsMemberAttribute( newV ) {
        this._setBooleanAttribute( 'is-member', newV );
        this._setMemberHooks();
        this._dispatchChange();
    }  

    get closed() {
        return this.hasAttribute( "closed" );
    }

    set closed( isClosed ) {
        if ( typeof isClosed !== "boolean" ) {
            return;
        }
        this._setClosedAttribute( isClosed );
    }

    _setClosedAttribute( newV ) {
        this._setBooleanAttribute( 'closed', newV );
    }
    
    get simpleForm() {
        return this.hasAttribute( "simple-form" );
    }

    set simpleForm( isSimple ) {
        if ( typeof isSimple !== "boolean" ) {
            return;
        }
        this._setSimpleFormAttribute( isSimple );
    }

    _setSimpleFormAttribute( newV ) {
        this._setBooleanAttribute( 'simple-form', newV );
    }

    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////

    _updateRegistrationObject( e ) {
        this._eventRegistrationServiceObject.registrationInfo = this._copy.deepCopy( e.detail.registrationInfo );
        this._dispatchChange();
    }

    _updateContactInfo( e ) {
        this._eventRegistrationServiceObject['contactFirstName'] = e.detail.contactInfo.contactFirstName;
        this._eventRegistrationServiceObject['contactLastName'] = e.detail.contactInfo.contactLastName;
        this._eventRegistrationServiceObject['contactEmail'] = e.detail.contactInfo.contactEmail;
        this._eventRegistrationServiceObject['contactPhone'] = e.detail.contactInfo.contactPhone;
        this._eventRegistrationServiceObject['companyName'] = e.detail.contactInfo.companyName;
        this._eventRegistrationServiceObject['contactTitle'] = e.detail.contactInfo.contactTitle;
        this._eventRegistrationServiceObject['howHear'] = e.detail.contactInfo.howHear;
        this._storedContactInfoEvent = e;
        this._dispatchChange();
    }

    _addRegistrationInfoHook( e ) {
        e.stopPropagation();
        let parentItemGroup = e.detail.parentItemGroup;
        this.addEventListener( 'contact-info-change', ( e ) => {
            parentItemGroup._contactInfoChangedHook( e ) 
        } );
        if( this._storedContactInfoEvent && parentItemGroup._contactInfoChangedHook ) {
            parentItemGroup._contactInfoChangedHook( this._storedContactInfoEvent );
        }
    }

    _addMemberHook( e ) {
        e.stopPropagation();
        if( !e.detail ) {
            this._memberHooks.push( e.target );
        } else {
            this._memberHooks.push( e.detail );
        }
        this._setMemberHooks();
    }

    _setMemberHooks() {
        if( !this._memberHooks || this._memberHooks.length === 0 ) {
            return;
        }
        for( let i = 0; i < this._memberHooks.length; i++ ) {
            let hook = this._memberHooks[i];
            if( hook ) {
                hook.isMember = this.isMember
            }
        }
    }

    _addInlineHook( e ) {
        e.stopPropagation();
        if( !e.detail ) {
            this._inlineHooks.push( e.target );
        } else {
            this._inlineHooks.push( e.detail );
        }
        this._setInlineHooks();
    }  
    
    _setInlineHooks() {
        for( let i = 0; i < this._inlineHooks.length; i++ ) {
            let hook = this._inlineHooks[i];
            if( hook ) {
                hook.inline = this.inline
            }
        }
    }

    _addSimpleFormHook( e ) {
        e.stopPropagation();
        if( !e.detail ) {
            this._simpleFormHooks.push( e.target );
        } else {
            this._simpleFormHooks.push( e.detail );
        }
        this._setSimpleFormHooks();
    }  
    
    _setSimpleFormHooks() {
        for( let i = 0; i < this._simpleFormHooks.length; i++ ) {
            let hook = this._simpleFormHooks[i];
            if( hook ) {
                hook.simpleForm = this.simpleForm
            }
        }
    }

    _updateTotals( subtotal ) {
        this._subtotal = subtotal;
        this._tax = this._review.calculateTax( this._subtotal );
        this._total = this._review.calulateAndDisplayOrderTotal( this._subtotal );
        this._dispatchChange();
    }
    
    _dispatchChange() {
        this.shadowRoot.dispatchEvent( new CustomEvent(
            'odp-event-registration-form-change',
            {
                bubbles: true,
                composed: true,
                'detail': this.getData()
            } 
        ) );
    } 

    _generateEventOrderId() {
        var orderId = 'evr';
        var currentTime = new Date();
        orderId = orderId 
                    + currentTime.getFullYear() 
                    + parseInt( currentTime.getMonth() + 1 ) 
                    + currentTime.getDate() 
                    + currentTime.getHours() 
                    + currentTime.getMinutes() 
                    + currentTime.getSeconds() 
                    + currentTime.getMilliseconds();
        this.orderId = orderId;
        return this.orderId;
    }
    

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////

    getData() {
        return {
            'eventRegistrationServiceObject': this._eventRegistrationServiceObject,
            'totals': {
                'subtotal': this._subtotal,
                'tax': this._tax,
                'total': this._total
            }
        }
    }

    validate() { 
        let valid = true;
        if( !this._builder.validate() ) {
            valid = false;
            this._invalidMessage = this._builder.invalidMessage;
        }
        if( !this._contact.validate() ) {
            this._invalidMessage = 'Please fill out Contact Information.';
            valid = false;
        }
        return valid;            
    }
}

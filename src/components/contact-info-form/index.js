import { ODPContactInfoForm } from "./ContactInfoForm.js";
if ( !customElements.get( "odp-contact-info-form" ) ) {
    window.customElements.define( "odp-contact-info-form", ODPContactInfoForm );
}
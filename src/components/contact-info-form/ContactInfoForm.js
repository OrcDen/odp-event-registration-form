import '@orcden/od-forms';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>
    
        :host {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
    
        #contact-form::part( field-label ) {
            white-space: pre;
        }

    </style>
    
    <od-form id='contact-form'>
        <od-form-input
                required
                name='contactFirstName'>First Name</od-form-input>
        <od-form-input
                required
                name='contactLastName'>Last Name</od-form-input>
        <od-form-input
                required
                type='email'
                name='contactEmail'>Email</od-form-input>
        <od-form-input
                type='tel'
                name='contactPhone'>Phone</od-form-input>
        <od-form-input
                name='companyName'>Company Name</od-form-input>
        <od-form-input
                name='contactTitle'>Job Title</od-form-input>
        <od-form-input name='howHear'>How Did You Hear<br> About This Event?</od-form-input>
    </od-form>
    <p>Note: The email address above will be used to send you a receipt of your purchase</p>        
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "odp-contact-info-form" );

export class ODPContactInfoForm extends HTMLElement {
    constructor() {
        super();
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {

        this._form = this.shadowRoot.querySelector( '#contact-form' );
        this._contactFirstNameInput = this.shadowRoot.querySelector( 'od-form-input[name="contactFirstName"]' );
        this._contactLastNameInput = this.shadowRoot.querySelector( 'od-form-input[name="contactLastName"]' );
        this._contactEmailInput = this.shadowRoot.querySelector( 'od-form-input[name="contactEmail"]' );
        this._contactPhoneInput = this.shadowRoot.querySelector( 'od-form-input[name="contactPhone"]' );
        this._companyNameInput = this.shadowRoot.querySelector( 'od-form-input[name="companyName"]' );
        this._contactTitleInput = this.shadowRoot.querySelector( 'od-form-input[name="contactTitle"]' );
        this._howHearInput = this.shadowRoot.querySelector( 'od-form-input[name="howHear"]' );

        //set attribute default values first and pass to element

        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( 'inline' );
        this._upgradeProperty( 'companyRequired' );
        this._upgradeProperty( 'phoneRequired' );

        //listeners and others etc.
        this._form.form.addEventListener( 'submit', ( e ) => { 
            e.stopPropagation();
            e.preventDefault();
            this.validate();
            return false;
        } );
        
        this._form.form.addEventListener( 'change', ( e ) => { this._notifyContactChanged( e ) } );
        
        this._form.setAttribute( 'exportparts', 'form, table, table-row, table-cell, label-cell, input-cell, field-label, field-label-row, value-label ,form-input, form-select, form-textarea, form-richtext, richtext-editor' );
        this.setAttribute( 'exportparts', 'form : contact-form, table : contact-table, table-row : contact-table-row, table-cell : contact-table-cell, label-cell : contact-label-cell, input-cell : contact-input-cell, field-label : contact-field-label, field-label-row : contact-field-label-row, value-label : contact-value-label, form-input : contact-form-input, form-select : contact-form-select, form-textarea : contact-form-textarea, form-richtext : contact-form-richtext, richtext-editor : contact-richtext-editor' );
    }

    //General - every element should have this
    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ['inline','company-required','phone-required'];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        switch ( attrName ) {
            case "inline": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setInlineAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setInlineAttribute( newValue );
                }
                break;
            }
            case "company-required": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setCompanyRequiredAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setCompanyRequiredAttribute( newValue );
                }
                break;
            }
            case "phone-required": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setPhoneRequiredAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setPhoneRequiredAttribute( newValue );
                }
                break;
            }
        }
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }

    get inline() {
        return this.hasAttribute( "inline" );
    }

    set inline( isInline ) {
        if ( typeof isInline !== "boolean" ) {
            return;
        }
        this._setInlineAttribute( isInline );
    }

    _setInlineAttribute( newV ) {
        this._setBooleanAttribute( 'inline', newV );
        
        this._contactFirstNameInput.inline = this.inline;
        this._contactLastNameInput.inline = this.inline;
        this._contactEmailInput.inline = this.inline;
        this._contactPhoneInput.inline = this.inline;
        this._companyNameInput.inline = this.inline;
        this._contactTitleInput.inline = this.inline;
        this._howHearInput.inline = this.inline;
    }  

    get phoneRequired() {
        return this.hasAttribute( "phone-required" );
    }

    set phoneRequired( required ) {
        if ( typeof required !== "boolean" ) {
            return;
        }
        this._setPhoneRequiredAttribute( required );
    }

    _setPhoneRequiredAttribute( newV ) {
        this._setBooleanAttribute( 'phone-required', newV );
        this._contactPhoneInput.setAttribute( 'required', newV );
    }

    get companyRequired() {
        return this.hasAttribute( "company-required" );
    }

    set companyRequired( required ) {
        if ( typeof required !== "boolean" ) {
            return;
        }
        this._setCompanyRequiredAttribute( required );
    }

    _setCompanyRequiredAttribute( newV ) {
        this._setBooleanAttribute( 'company-required', newV );
        this._companyNameInput.setAttribute( 'required', newV );
    } 

    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////    

    _notifyContactChanged( e ) {
        this.shadowRoot.dispatchEvent( new CustomEvent(
            'contact-info-change', {
                bubbles: true,
                composed: true,
                'detail': {
                    'contactInfo': this.getData()
                }
            }
        ) );
    }

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////

    validate() {
        if( !this._form.validate() ) {
            this._form.htmlValidate();
            return false;
        }
        return true;
    }

    getData() {
        return this._form.getData();
    }
}

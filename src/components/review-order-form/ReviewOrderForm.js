import '@orcden/od-forms';

const template = document.createElement( "template" );
template.innerHTML = `
    <style>

        :host {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            width: 100%;
            contain: content;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
    
        #review-order-form::part( form-input ) {
            border: none;
        }

    </style>
    
    <od-form id='review-order-form'>
        <od-form-input 
                readonly
                inline
                name='subtotal'>
            <label>Order SubTotal</label>
        </od-form-input>
        <od-form-input 
                readonly
                inline
                name='tax'>
            <label>Tax: 13% (HST)</label>
        </od-form-input>
        <od-form-input 
                readonly 
                inline
                name='orderTotal'>
            <label>Cost of Order</label>
        </od-form-input>
    </od-form>    
`;

window.ShadyCSS && window.ShadyCSS.prepareTemplate( template, "odp-review-order-form" );

export class OdpReviewOrderForm extends HTMLElement {
    constructor() {
        super();
        this._ready = false;
        //constructor creates the shadowRoot
        window.ShadyCSS && window.ShadyCSS.styleElement( this );
        if ( !this.shadowRoot ) {
            this.attachShadow( { mode: "open" } );
            this.shadowRoot.appendChild( template.content.cloneNode( true ) );
        }
    }

    connectedCallback() {

        this._form = this.shadowRoot.querySelector( '#review-order-form' );
        this._subtotalInput = this.shadowRoot.querySelector( 'od-form-input[name="subtotal"]' );
        this._taxInput = this.shadowRoot.querySelector( 'od-form-input[name="tax"]' );
        this._orderTotalInput = this.shadowRoot.querySelector( 'od-form-input[name="orderTotal"]' );

        //set attribute default values first and pass to element
        if( !this.hasAttribute( 'tax' ) ) {
            this.setAttribute( 'tax', 0 );
        }

        //all properties should be upgraded to allow lazy functionality
        this._upgradeProperty( 'noTax' );

        //listeners and others etc.

        this._form.setAttribute( 'exportparts', 'form, table, table-row, table-cell, label-cell, input-cell, field-label, field-label-row, value-label ,form-input, form-select, form-textarea, form-richtext, richtext-editor' );
        this.setAttribute( 'exportparts', 'form : review-form, table : review-table, table-row : review-table-row, table-cell : review-table-cell, label-cell : review-label-cell, input-cell : review-input-cell, field-label : review-field-label, field-label-row : review-field-label-row, value-label : review-value-label, form-input : review-form-input, form-select : review-form-select, form-textarea : review-form-textarea, form-richtext : review-form-richtext, richtext-editor : review-richtext-editor' );
    }

    //General - every element should have this
    _upgradeProperty( prop ) {
        if ( Object.prototype.hasOwnProperty.call( this, prop ) ) {
            var value = this[prop];
            delete this[prop];
            this[prop] = value;
        }
    }

    ////////////////////////////////
    //
    // Override methods
    //
    ////////////////////////////////

    static get observedAttributes() {
        //only observe the attributes you expect to change internally/externally
        return ['no-tax'];
    }

    attributeChangedCallback( attrName, oldValue, newValue ) {
        //handle side effects/extra logic - DO NOT SET the associated property here
        //handle invald attribute values
        switch ( attrName ) {
            case "no-tax": {
                if ( newValue !== null && newValue !== "" && !this._validateBoolean( newValue ) ) { //boolean type safety: null - removed, "" - set true
                    this._setNoTaxAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setNoTaxAttribute( newValue );
                }
                break;
            }
            case "tax": {
                if ( newValue !== null && newValue !== "" && !this._validateTax( newValue ) ) {
                    this._setTaxAttribute( oldValue );
                    break;
                }
                if( newValue !== oldValue ) {
                    this._setTaxAttribute( newValue );
                }
                break;
            }
        }
    }

    ////////////////////////////////
    //
    // Properties - all 'custom attributes' should have a getter and setter to reflect the attribute
    //
    ////////////////////////////////

    _validateBoolean( newV ) {
        return (
            typeof newV === "boolean" || newV === "true" || newV === "false"
        );
    }

    _setBooleanAttribute( name, newV ) {
        if ( newV !== "" && ( !newV || newV === "false" ) ) {
            this.removeAttribute( name );
        } else {
            this.setAttribute( name, true );
        }
    }    

    _validateFloat( newV ) {
        return (
            typeof newV === "number" || Number.parseFloat( newV ) !== NaN
        );
    }

    get noTax() {
        return this.hasAttribute( "inline" );
    }

    set noTax( noTax ) {
        if ( typeof noTax !== "boolean" ) {
            return;
        }
        this._setNoTaxAttribute( noTax );
    }

    _setNoTaxAttribute( newV ) {
        this._setBooleanAttribute( 'no-tax', newV );
    }

    get tax() {
        return Number.parseFloat( this.getAttribute( "tax" ) );
    }

    set tax( tax ) {
        if ( typeof tax !== "number" && tax !== null ) {
            return;
        }
        this._setTaxAttribute( tax );
    }

    _setTaxAttribute( tax ) {        
        if ( tax === "" || tax === null ) {
            this.setAttribute( 'tax', 0 );
        } else {
            this.setAttribute( 'tax', tax );
        }
        this.noTax = !this.tax;
    }

    _validateTax( newV ) {
        return this._validateFloat( newV ) && Number.parseFloat( newV ) < 1;
    }

    ////////////////////////////////
    //
    // Listeners
    //
    ////////////////////////////////
    

    ////////////////////////////////
    //
    // Internal methods
    //
    ////////////////////////////////    

    _updateReviewPanel( subtotal, hst, orderTotal ) {
        let data = {};
        data['subtotal'] = subtotal;
        data['tax'] = hst;
        data['orderTotal'] = orderTotal;
        this._form.populate( data );
    }

    _notifyPriceChange( subTotal, hst, totalCost ) {
        this.shadowRoot.dispatchEvent( new CustomEvent(
            'price-update', {
                bubbles: true, 
                composed: true, 
                'detail': {
                    'subTotal': subTotal,
                    'hst': hst,
                    'totalCost': totalCost
                }
            }
        ) );
    }

    ////////////////////////////////
    //
    // Public methods
    //
    ////////////////////////////////

    calulateAndDisplayOrderTotal( subTotal ) {
        subTotal = Math.round(  Number( subTotal ) * 100 ) / 100;
        let hst = 0;
        if( !this.noTax ) {
            hst = this.calculateTax( subTotal );
        }
        let totalCost = Number( subTotal + hst ).toFixed( 2 );
        subTotal = subTotal.toFixed( 2 );
        hst = hst.toFixed( 2 );

        this._updateReviewPanel( subTotal, hst, totalCost );
        return Number( totalCost );
    }

    calculateTax( subTotal ) {
        subTotal = Number( subTotal );
        return  Math.round(  Number( subTotal * this.tax ) * 100 ) / 100;
    }
}
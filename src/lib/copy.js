export default class Copy {
    deepCopy( object ) {
        let temp = object;
        let newObject = {};
        for( let i = 0; i < Object.keys( temp ).length; i++ ) {
            let field = Object.keys( temp )[i];
            if( temp[field] !== null && temp[field] !== undefined ) {
                if( Array.isArray( temp[field] ) ) {
                    newObject[field] = this.copyArray( temp[field] )
                } else if( typeof temp[field] === 'object' ) {
                    newObject[field] = this.deepCopy( temp[field] )
                } else {
                    newObject[field] = temp[field];
                } 
            }
        }
        return newObject;
    }
    
    copyArray( array ) {
        let temp = array;
        let newArray = []
        for( let i = 0; i < temp.length; i++ ) {
            let elem = temp[i];
            if( elem !== null && elem != undefined ) {
                if( Array.isArray( elem ) ) {
                    newArray.push( this.copyArray( elem ) )
                } else if ( typeof elem === 'object' ) {
                    newArray.push( this.deepCopy( elem ) )
                } else {
                    newArray.push( elem )
                }
            }
        }
        return newArray;
    }
}

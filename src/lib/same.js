export default class Same {
    isSame( object, compare ) {
        if( object === undefined && compare !== undefined ) {
            return false;
        }
        if( object === null && compare !== null ) {
            return false;
        }
        if( typeof object !== typeof compare ) {
            return false; 
        }
        if( Array.isArray( object ) ) {
            return this.arraySame( object, compare );
        }
        if( typeof object !== 'object' ) {
            return object === compare;
        }
        if( Object.keys( object ).length !== Object.keys( compare ).length ) {
            return false;
        }
        for( let i = 0; i < Object.keys( object ).length; i++ ) {
            let field = Object.keys( object )[i];
            let compareField = Object.keys( compare )[i];
            if( field !== compareField ) {
                return false;
            }
            if( !this.isSame( object[field], compare[compareField] ) ) {
                return false;
            }
        }
        return true;
    }
    
    arraySame( array, compare ) {
        if( array === undefined && compare !== undefined ) {
            return false;
        }
        if( array === null && compare !== null ) {
            return false;
        }
        if( !Array.isArray( array ) || !Array.isArray( compare ) ) {
            return false; 
        }
        if( array.length !== compare.length ) {
            return false;
        }
        for( let i = 0; i < array.length; i++ ) {
            if( !this.isSame( array[i], compare[i] ) ) {
                return false;
            }
        }
        return true;
    }
}